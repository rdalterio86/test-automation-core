package com.test.automation.mock;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.sshd.common.Factory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.CommandFactory;
import org.apache.sshd.server.Environment;
import org.apache.sshd.server.ExitCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FakeCommandFactory implements CommandFactory {
	private static final String DIR = "dir";
	private static final String CD = "cd";

	@Override
	public Command createCommand(String command) {
		return new FakeFactory(command).create();
	}

	public static class FakeFactory implements Factory<Command> {
		private String cmd;

		FakeFactory(String cmd) {
			this.cmd = cmd;
		}

		@Override
		public Command create() {
			return new FakeCommand(cmd);
		}
	}

	public static class FakeCommand implements Command, Runnable {
		private String cmd;
		private InputStream in;
		private OutputStream out;
		private OutputStream err;
		private ExitCallback callback;
		private Thread thread;
		private static final Logger LOGGER = LoggerFactory.getLogger(FakeCommand.class);

		FakeCommand(String cmd) {
			this.cmd = cmd;
		}

		@Override
		public void setInputStream(InputStream in) {
			this.in = in;
		}

		@Override
		public void setOutputStream(OutputStream out) {
			this.out = out;
		}

		@Override
		public void setErrorStream(OutputStream err) {
			this.err = err;
		}

		@Override
		public void setExitCallback(ExitCallback callback) {
			this.callback = callback;
		}

		@Override
		public void start(Environment env) throws IOException {
			this.thread = new Thread(this, "simple-ssh");
			this.thread.start();
		}

		@Override
		public void destroy() throws Exception {
			this.thread.interrupt();
		}

		@Override
		public void run() {
			String[] splitCommands = this.cmd.split("&&");
			boolean isFailure = false;
			for (String command : splitCommands) {
				if (StringUtils.contains(command, DIR) || StringUtils.contains(command, CD)) {
					LOGGER.info("Command:" + this.cmd);
					PrintWriter p = new PrintWriter(this.out, true);
					p.println(new String("Command:" + this.cmd));
					p.flush();
					p.close();
				} else {
					LOGGER.info("Command [" + this.cmd + "] not valid!");
					isFailure = true;
					PrintWriter p = new PrintWriter(this.err, true);
					p.println(new String("Command [" + this.cmd + "] not valid!"));
					p.flush();
					p.close();
					break;
				}
			}
			if (isFailure) {
				this.callback.onExit(1);
			} else {
				this.callback.onExit(0);
			}
		}
	}
}
