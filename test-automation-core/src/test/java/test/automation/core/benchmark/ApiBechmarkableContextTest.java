package test.automation.core.benchmark;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import test.automation.core.benchmark.ApiBenchmarkableContext;
import test.automation.core.benchmark.BenchmarkableAnnotationObject;
import test.automation.core.benchmark.BenchmarkableContext;

public class ApiBechmarkableContextTest {
	
	@Rule
	public ErrorCollector collector = new ErrorCollector();
	
	@Test
	public void testApiBenchmarkableContextWithFullAnnotation(){
		String annotation = "@benchmarkable(name=\"testCreatePractice\", path=\"/practices/create\", method=\"POST\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation);
		BenchmarkableContext ctx = new ApiBenchmarkableContext(annotations,null);
		List<BenchmarkableAnnotationObject> objectList = ctx.getBenchmarkableObjectList();
		
		
		for(BenchmarkableAnnotationObject obj : objectList ) {
		collector.checkThat(obj, is(notNullValue()));
		collector.checkThat(obj.getName(), CoreMatchers.is("testCreatePractice"));
		collector.checkThat(obj.getPath(), CoreMatchers.is("/practices/create"));
		
		}
	}
	
	@Test
	public void testApiBenchmarkableContextMultipleFullAnnotations(){
		String annotation1 = "@benchmarkable(name=\"testCreatePractice\", path=\"/practices/create\", method=\"POST\")";
		String annotation2 = "@benchmarkable(name=\"testReadPractice\", path=\"/practices/code\", method=\"GET\")";
		String annotation3 = "@benchmarkable(name=\"testReadPractice2\", path=\"/practices/code2\", method=\"POST\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation1);
		annotations.add(annotation2);
		annotations.add(annotation3);
		BenchmarkableContext ctx = new ApiBenchmarkableContext(annotations,null);
		
		List<BenchmarkableAnnotationObject> benchmarkableObjectList = ctx.getBenchmarkableObjectList();
		Assert.assertEquals(3, benchmarkableObjectList.size());
		benchmarkableObjectList.stream().forEach(s->System.out.println(s));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testApiBenchmarkableContextExpectedExceptionMissedPath(){
		String annotation = "@benchmarkable(name=\"testCreatePractice\", path=\"\", method=\"POST\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation);
		
		BenchmarkableContext ctx = new ApiBenchmarkableContext(annotations,null);
			
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testApiBenchmarkableContextExpectedExceptionMissedMethod(){
		String annotation = "@benchmarkable(name=\"testCreatePractice\", path=\"/practices/create\", method=\"\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation);
		
		BenchmarkableContext ctx = new ApiBenchmarkableContext(annotations,null);
		        
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testApiBenchmarkableContextExpectedExceptionMissedName(){
		String annotation = "@benchmarkable(name=\"\", path=\"/practices/create\", method=\"POST\")";
		List<String> annotations = new ArrayList<>();
		annotations.add(annotation);
		
		BenchmarkableContext ctx = new ApiBenchmarkableContext(annotations,null);
		
	}
	
}
