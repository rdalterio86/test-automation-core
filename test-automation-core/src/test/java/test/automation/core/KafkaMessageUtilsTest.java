package test.automation.core;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.kafka.test.hamcrest.KafkaMatchers.hasKey;
import static org.springframework.kafka.test.hamcrest.KafkaMatchers.hasPartition;
import io.confluent.kafka.schemaregistry.client.MockSchemaRegistryClient;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import test.automation.core.MessageUtils;
import test.automation.core.kafka.AvroProducerRecordUtils;
import test.automation.core.kafka.KafkaConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.commons.io.IOUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;

public class KafkaMessageUtilsTest {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(KafkaMessageUtilsTest.class);

	private static String SENDER_TOPIC = "testTopic";
	
	private static String LOCAL_HOST = "http://localhost:8081";
	
	private static String MOCK_SERIALIZER_AVRO = "com.oneleo.test.automation.mock.KafkaAvroSerializerMock";
	
	private static String AVRO_SERIALIZATION = "avro";
	
	private static String DEFAULT_KAFKA_PROPERTIES = "default-kafka.properties";
	
	private static String SERVER_KAFKA_PROPERTIES = "server-kafka.properties";

	private static KafkaMessageListenerContainer<Integer, Object> container;

	private static BlockingQueue<ConsumerRecord<Object, Object>> records;

	private static File defaultKafkaPropertiesFile;
	
	private static File serverKafkaPropertiesFile;
	
	private static String jsonSchema = "{\"type\": \"record\"," + "\"name\": \"evolution\","
			+ " \"namespace\": \"com.landoop\"," + "\"fields\": ["
			+ " {\"name\": \"name\", \"type\": \"string\"" + "},"
			+ " {" + "\"name\": \"number1\","
			+ " \"type\": \"int\"" + "},"
			+ " {" + "\"name\": \"number2\","
			+ " \"type\": \"float\"" + " }"
			+ " ]" + "}";

	@ClassRule
	public static KafkaEmbedded embeddedKafka = new KafkaEmbedded(1, true,
			SENDER_TOPIC);
	
	@ClassRule
	public static TemporaryFolder tempFolder= new TemporaryFolder();

	@BeforeClass
	public static void setUp() {
	
	try {

			Properties props = new Properties();
			props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
					embeddedKafka.getBrokersAsString());
			props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
					IntegerSerializer.class.getName());
			props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
					MOCK_SERIALIZER_AVRO);
			props.setProperty(KafkaConstants.SERIALIZATION_TYPE, AVRO_SERIALIZATION);
			props.setProperty(KafkaConstants.SCHEMA_REGISTRY_URL,
					LOCAL_HOST);
				
			defaultKafkaPropertiesFile = tempFolder.newFile(DEFAULT_KAFKA_PROPERTIES);
			
			if(defaultKafkaPropertiesFile.exists()){
				defaultKafkaPropertiesFile.delete();
			}
	
			FileOutputStream fosDeafult = new FileOutputStream(defaultKafkaPropertiesFile);
			props.store(fosDeafult, "");
			IOUtils.closeQuietly(fosDeafult);
			
			serverKafkaPropertiesFile = tempFolder.newFile(SERVER_KAFKA_PROPERTIES);
			
			if(serverKafkaPropertiesFile.exists()){
				serverKafkaPropertiesFile.delete();
			}
	
			FileOutputStream fosServer = new FileOutputStream(serverKafkaPropertiesFile);
			props.store(fosServer, "");
			IOUtils.closeQuietly(fosServer);
	
			System.setProperty("absoluteConfPath",
					defaultKafkaPropertiesFile.getParent());
	
			// set up the Kafka consumer properties
			Map<String, Object> consumerProperties = KafkaTestUtils.consumerProps(
					"sender", "false", embeddedKafka);
			
			consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class);
			consumerProperties.put(KafkaConstants.SCHEMA_REGISTRY_URL, LOCAL_HOST);
			
			MockSchemaRegistryClient client = new MockSchemaRegistryClient();
			Schema schema = new Schema.Parser().parse(jsonSchema);
			client.register("testSchema", schema);
			Deserializer<Object> deserializer = new KafkaAvroDeserializer(client);
			
			// create a Kafka consumer factory
			DefaultKafkaConsumerFactory<Integer, Object> consumerFactory = new DefaultKafkaConsumerFactory<Integer, Object>(
					consumerProperties, new IntegerDeserializer(), deserializer);
	
			// set the topic that needs to be consumed
			ContainerProperties containerProperties = new ContainerProperties(
					SENDER_TOPIC);
	
			// create a Kafka MessageListenerContainer
			container = new KafkaMessageListenerContainer<>(consumerFactory,
					containerProperties);
	
			// create a thread safe queue to store the received message
			records = new LinkedBlockingQueue<>();
	
			// setup a Kafka message listener
			container.setupMessageListener(new MessageListener<Object, Object>() {
				@Override
				public void onMessage(ConsumerRecord<Object, Object> record) {
					LOGGER.info("test-listener received message='{}'",
							record.toString());
					records.add(record);
				}
			});
	
			// start the container and underlying message listener
			container.start();
	
			// wait until the container has the required number of assigned
			// partitions
			ContainerTestUtils.waitForAssignment(container,
					embeddedKafka.getPartitionsPerTopic());
      
	  } catch (Exception e) {
		throw new RuntimeException(e);
    }
		
  }
	  
	@Test 
	public void testGetTemplate() {
	 
		 assertNotNull(MessageUtils.kafka().template());
	  
	 }
	
	@Test
	public void testGetTemplateByServerId() {
		assertNotNull(MessageUtils.kafka().template("server"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetTemplateByServerIdKO() {
		
		assertNotNull(MessageUtils.kafka().template("server2"));
		
	}

	@Test
	public void testSendAndReceive() {
  
	try {
		
		String json = "{\"name\":\"test name22\", \"number1\":24, \"number2\":26.26}";
		
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("TestHeaderKey", "Test Header Value");
		RecordHeaders headers = AvroProducerRecordUtils.createRecordHeaders(headerMap);
		
		ProducerRecord<Integer, GenericRecord> record = produceRecord(SENDER_TOPIC,jsonSchema,json,headers,0,2);

		MessageUtils.<Integer, GenericRecord>kafka().template().send(record);

		ConsumerRecord<Object, Object> received = records.poll(10, TimeUnit.SECONDS);
		assertThat(received, hasPartition(0));
		assertThat(received, hasKey(2));
		assertEquals(json.replaceAll("\\s", ""), received.value().toString().replaceAll("\\s", ""));
		assertEquals(headers.toString().replaceAll("\\s", ""), received.headers().toString().replaceAll("\\s", ""));
      
	   } catch (Exception e) {
		  throw new RuntimeException(e);
     }
		
   }

	@AfterClass
	public static void tearDown() {
		
  try {
		// stop the container
		container.stop();
     } catch (Exception e) {
			throw new RuntimeException(e);
		}
  
	if (defaultKafkaPropertiesFile != null) {
		defaultKafkaPropertiesFile.delete();
	}
	
	if (serverKafkaPropertiesFile != null) {
		serverKafkaPropertiesFile.delete();
	}
		
		System.clearProperty("absoluteConfPath");
	}

	private ProducerRecord<Integer, GenericRecord> produceRecord(String topicName,
			String jsonSchema, String json, RecordHeaders headers,
			Integer partition, Integer key) throws IOException {
		Schema schema = new Schema.Parser().parse(jsonSchema);

		// Get the json decoder, this is used to parse a json and to get the
		// record
		Decoder decoder = DecoderFactory.get().jsonDecoder(schema, json);
		DatumReader<GenericData.Record> reader = new GenericDatumReader<>(
				schema);
		GenericRecord datum = reader.read(null, decoder);

		// Instantiate the record
		return new ProducerRecord<Integer, GenericRecord>(topicName, partition, key,
				datum, headers);
	}

}