package test.automation.core.graph;

import java.io.InputStream;

import org.apache.tinkerpop.gremlin.server.GremlinServer;
import org.apache.tinkerpop.gremlin.server.Settings;

/**
 * Creates a re-usable instance of a server. You may want to @Before and @After
 * to prepare and/or clean up state in your tests.
 * 
 *
 */
public class GremlinServerHelper {

	private GremlinServer server;

	public void startServer() throws Exception {
		final InputStream stream = this.getClass().getResourceAsStream("/graph/gremlin-server.yaml");
		this.server = new GremlinServer(Settings.read(stream));

		server.start().join();
	}

	public void stopServer() throws Exception {
		server.stop().join();
	}
}
