package test.automation.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.automation.core.DatabaseUtils.jdbc;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JdbcDriverNameUtilsTest {
	
	private boolean isDatabaseInitialized = false;
	private String query = "";
	private SqlRowSet rowSet1 = null;
	private SqlRowSet rowSet2 = null;
	private SqlRowSet rowSet3 = null;
	
	@Before
	public void setup() {

		System.setProperty("confPath", "src/test/resources");
		
	}

	@Test
	public void testDriverClassNameJdbc() {
		
		initializeProductDatabase();
		
		query = "select * from product where id=?";
		rowSet1 = jdbc().template("product").queryForRowSet(query,"00010");
		rowSet2 = jdbc().template("product").queryForRowSet(query,"00020");
		rowSet3 = jdbc().template("product").queryForRowSet(query,"00030");
		
		assertTrue(rowSet1.next());
		assertTrue(rowSet2.next());
		assertTrue(rowSet3.next());
		
		assertEquals("Apple IPhone 6",rowSet1.getString("description"));
		assertEquals("Samsung Galaxy 7 Edge",rowSet2.getString("description"));
		assertEquals("LG G3",rowSet3.getString("description"));
		
		assertEquals(600,rowSet1.getInt("price"));
		assertEquals(800,rowSet2.getInt("price"));
		assertEquals(400,rowSet3.getInt("price"));
		
		cleanProductDatabase();
		
	}
	
	@Test
	public void testDataSourceClassNameJdbc() {
		
		initializeCustomerDatabase();
		
		query = "select * from customer where id=?";
		rowSet1 = jdbc().template("customer").queryForRowSet(query,"000111");
		rowSet2 = jdbc().template("customer").queryForRowSet(query,"000222");
		rowSet3 = jdbc().template("customer").queryForRowSet(query,"000333");
		
		assertTrue(rowSet1.next());
		assertTrue(rowSet2.next());
		assertTrue(rowSet3.next());
		
		assertEquals("11",rowSet1.getString("companyCode"));
		assertEquals("22",rowSet2.getString("companyCode"));
		assertEquals("33",rowSet3.getString("companyCode"));
		
		assertEquals("ALLIANCE_HEALTHCARE_1",rowSet1.getString("companyName"));
		assertEquals("ALLIANCE_HEALTHCARE_2",rowSet2.getString("companyName"));
		assertEquals("ALLIANCE_HEALTHCARE_3",rowSet3.getString("companyName"));
		
		assertEquals("ALLIANCE_Global_1",rowSet1.getString("trustName"));
		assertEquals("ALLIANCE_Global_2",rowSet2.getString("trustName"));
		assertEquals("ALLIANCE_Global_3",rowSet3.getString("trustName"));
		
		cleanCustomerDatabase();
		
	}
	
	private void initializeProductDatabase() {
		
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();

		populator.addScripts(
				new ClassPathResource("sql-scripts/product-test-schema.sql"),
				new ClassPathResource("sql-scripts/product-initial-load.sql"));
		populator.execute(jdbc().template("product").getDataSource());
		
		isDatabaseInitialized = true; 
		
	}
	
	private void cleanProductDatabase() {
		
		if (isDatabaseInitialized) {
			
			jdbc().template("product").execute("DROP TABLE PRODUCT");
			
		}
	}
	
	private void initializeCustomerDatabase() {
		
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();

		populator.addScripts(
				new ClassPathResource("sql-scripts/customer-test-schema.sql"),
				new ClassPathResource("sql-scripts/customer-initial-load.sql"));
		populator.execute(jdbc().template("customer").getDataSource());
		
		isDatabaseInitialized = true; 
		
	}
	
	private void cleanCustomerDatabase() {
		
		if (isDatabaseInitialized) {
			
			jdbc().template("customer").execute("DROP TABLE CUSTOMER");
		}
			
	}

}
