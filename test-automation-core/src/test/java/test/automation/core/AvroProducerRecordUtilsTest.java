package test.automation.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.avro.Schema;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaMetadata;
import test.automation.core.kafka.AvroProducerRecordUtils;
import test.automation.core.kafka.KafkaConstants;
import test.automation.core.kafka.KafkaPropertiesReader;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ AvroProducerRecordUtils.class, CachedSchemaRegistryClient.class,KafkaPropertiesReader.class })
public class AvroProducerRecordUtilsTest {
	
	private static final String PRODUCER_RECORD_NULL = "Producer Record is null ";
	private static final String RESOURCE_NAME = "default";
	private static final String TOPIC_NAME = "topicTest";
	private static final String SCHEMA_NAME = "testSchema";
	private static final String JSON = "{\"name\":\"test name22\", \"number1\":24, \"number2\":26.26}";
	private static final String JSON_SCHEMA = "{\"type\": \"record\"," + "\"name\": \"evolution\"," + " \"namespace\": \"com.landoop\","
			+ "\"fields\": [" + " {\"name\": \"name\", \"type\": \"string\"" + "}," + " {"
			+ "\"name\": \"number1\"," + " \"type\": \"int\"" + "}," + " {" + "\"name\": \"number2\","
			+ " \"type\": \"float\"" + " }" + " ]" + "}";
	private static final String SCHEMA_NAME_KEY = "testSchemaKey";
	private static final String SCHEMA_NAME_VALUE= "testSchemaValue";
	private static final String JSON_SCHEMA_KEY = "{\n" + 
			"  \"type\": \"record\",\n" + 
			"  \"name\": \"evolution\",\n" + 
			"  \"namespace\": \"com.landoop\",\n" + 
			"  \"doc\": \"This is a sample Avro schema to get you started. Please edit\",\n" + 
			"  \"fields\": [\n" + 
			"    {\n" + 
			"      \"name\": \"name\",\n" + 
			"      \"type\": \"string\"\n" + 
			"    }\n" + 
			"  ]\n" + 
			"}";
	private static final String JSON_KEY = "{\"name\":\"test key\"}";
	
	private Map<String, String> headerMap = new HashMap<>();
	private RecordHeaders headers;
	private Integer partition = 1;
	private String key = "test";
	
	@Before
	public void setup() {
		headerMap.put("TestHeaderKey", "Test Header Value");
		headers = AvroProducerRecordUtils.createRecordHeaders(headerMap);
	}

	@Test
	public void testCreateAvroRecordWithValueSchemaAssociation() throws Exception {
		
		setUrl("http://localhost:9090");
		
		CachedSchemaRegistryClient schemaRegistryClient = PowerMockito.mock(CachedSchemaRegistryClient.class);
		PowerMockito.whenNew(CachedSchemaRegistryClient.class).withAnyArguments().thenReturn(schemaRegistryClient);
		Schema schema = new Schema.Parser().parse(JSON_SCHEMA);
		SchemaMetadata value = PowerMockito.mock(SchemaMetadata.class);
		PowerMockito.when(schemaRegistryClient.getLatestSchemaMetadata(Mockito.anyString())).thenReturn(value);
		PowerMockito.when(value.getId()).thenReturn(9);
		PowerMockito.when(schemaRegistryClient.getById(Mockito.anyInt())).thenReturn(schema);
		AvroProducerRecordUtils
		.createAvroRecordWithHeaders(RESOURCE_NAME, TOPIC_NAME, SCHEMA_NAME, JSON, headers, partition, key);
		
		//Test producer record without resource
		ProducerRecord<Object, Object> avroRecordDefaultWithHeaders = AvroProducerRecordUtils
				.createAvroRecordWithHeaders(TOPIC_NAME, SCHEMA_NAME, JSON, headers, partition, key);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecordDefaultWithHeaders);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecordDefaultWithHeaders.value().toString().replaceAll("\\s", ""));
		assertEquals(headers.toString().replaceAll("\\s", ""), avroRecordDefaultWithHeaders.headers().toString().replaceAll("\\s", ""));
		
		//Test producer record without headers, partition and key
		ProducerRecord<Object, Object> avroRecord = AvroProducerRecordUtils
				.createAvroRecord(RESOURCE_NAME, TOPIC_NAME, SCHEMA_NAME, JSON);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecord);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecord.value().toString().replaceAll("\\s", ""));
		
		//Test producer record without resource, headers, partition and key
		ProducerRecord<Object, Object> avroRecordDefault = AvroProducerRecordUtils
				.createAvroRecord(TOPIC_NAME, SCHEMA_NAME, JSON);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecordDefault);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecordDefault.value().toString().replaceAll("\\s", ""));

		//Test producer record without headers
		ProducerRecord<Object, Object> avroRecordWithoutHeaders = AvroProducerRecordUtils
				.createAvroRecord(RESOURCE_NAME, TOPIC_NAME, SCHEMA_NAME, JSON, partition, key);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecordWithoutHeaders);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecordWithoutHeaders.value().toString().replaceAll("\\s", ""));
		
		//Test producer record without headers and resource
		ProducerRecord<Object, Object> avroRecordWithoutHeadersAndResource = AvroProducerRecordUtils
				.createAvroRecord(TOPIC_NAME, SCHEMA_NAME, JSON, partition, key);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecordWithoutHeadersAndResource);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecordWithoutHeadersAndResource.value().toString().replaceAll("\\s", ""));		
		
	}

	@SuppressWarnings("unchecked")
	private void setUrl(String url) {
		PowerMockito.mockStatic(KafkaPropertiesReader.class);
		Map<String, Properties> read = PowerMockito.mock(HashMap.class);
		PowerMockito.when(KafkaPropertiesReader.read()).thenReturn(read);
		Properties property=PowerMockito.mock(Properties.class);
		PowerMockito.when(read.get(Mockito.anyString())).thenReturn(property);
		PowerMockito.when(read.containsKey(Mockito.anyString())).thenReturn(true);
		PowerMockito.when(property.getProperty(KafkaConstants.SCHEMA_REGISTRY_URL)).thenReturn(url);
	}
	
	@Test
	public void testCreateAvroRecordWithValueAndSchemaKeyAssociation() throws Exception {
		
		setUrl("http://localhost:7090");
		
		CachedSchemaRegistryClient schemaRegistryClient = PowerMockito.mock(CachedSchemaRegistryClient.class);
		PowerMockito.whenNew(CachedSchemaRegistryClient.class).withAnyArguments().thenReturn(schemaRegistryClient);
		Schema schemaKey = new Schema.Parser().parse(JSON_SCHEMA_KEY);
		Schema schemaValue = new Schema.Parser().parse(JSON_SCHEMA);
		SchemaMetadata value = PowerMockito.mock(SchemaMetadata.class);
		SchemaMetadata schemaMetadatakey = PowerMockito.mock(SchemaMetadata.class);
		
		PowerMockito.when(schemaRegistryClient.getLatestSchemaMetadata(SCHEMA_NAME_KEY)).thenReturn(schemaMetadatakey);
		PowerMockito.when(schemaRegistryClient.getLatestSchemaMetadata(SCHEMA_NAME_VALUE)).thenReturn(value);
		PowerMockito.when(value.getId()).thenReturn(9);
		PowerMockito.when(schemaMetadatakey.getId()).thenReturn(7);
		PowerMockito.when(schemaRegistryClient.getById(9)).thenReturn(schemaValue);
		PowerMockito.when(schemaRegistryClient.getById(7)).thenReturn(schemaKey);
		AvroProducerRecordUtils
		.createAvroRecordWithHeadersAndSchemaKey(TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, headers, partition, JSON_KEY);
		ProducerRecord<Object, Object> avroRecordWithHeaders = AvroProducerRecordUtils
				.createAvroRecordWithHeadersAndSchemaKey(TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, headers, partition, JSON_KEY);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecordWithHeaders);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecordWithHeaders.value().toString().replaceAll("\\s", ""));
		assertEquals(JSON_KEY.replaceAll("\\s", ""), avroRecordWithHeaders.key().toString().replaceAll("\\s", ""));
		assertEquals(headers.toString().replaceAll("\\s", ""), avroRecordWithHeaders.headers().toString().replaceAll("\\s", ""));
		
		//Test producer record without headers
		ProducerRecord<Object, Object> avroRecordWithSchenaKeyAndNoHeaders = AvroProducerRecordUtils
				.createAvroRecordWithSchemaKey(RESOURCE_NAME, TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, partition, JSON_KEY);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecordWithSchenaKeyAndNoHeaders);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecordWithSchenaKeyAndNoHeaders.value().toString().replaceAll("\\s", ""));
		assertEquals(JSON_KEY.replaceAll("\\s", ""), avroRecordWithSchenaKeyAndNoHeaders.key().toString().replaceAll("\\s", ""));
		
		//Test producer record without headers and Resource
		ProducerRecord<Object, Object> avroRecordDefaultWithSchenaKeyAndNoHeaders = AvroProducerRecordUtils
				.createAvroRecordWithSchemaKey(TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, partition, JSON_KEY);
		assertNotNull(PRODUCER_RECORD_NULL, avroRecordDefaultWithSchenaKeyAndNoHeaders);
		assertEquals(JSON.replaceAll("\\s", ""), avroRecordDefaultWithSchenaKeyAndNoHeaders.value().toString().replaceAll("\\s", ""));
		assertEquals(JSON_KEY.replaceAll("\\s", ""), avroRecordDefaultWithSchenaKeyAndNoHeaders.key().toString().replaceAll("\\s", ""));
		
	}	
	
	@SuppressWarnings("unchecked")
	@Test(expected = IllegalArgumentException.class)
	public void testCreateAvroRecordWithResourceNotSetFailure() throws Exception {
		PowerMockito.mockStatic(KafkaPropertiesReader.class);
		Map<String, Properties> read = PowerMockito.mock(HashMap.class);
		PowerMockito.when(KafkaPropertiesReader.read()).thenReturn(read);
		Properties property=PowerMockito.mock(Properties.class);
		PowerMockito.when(read.get(Mockito.anyString())).thenReturn(property);
		PowerMockito.when(read.containsKey(Mockito.anyString())).thenReturn(false);
		AvroProducerRecordUtils
		.createAvroRecordWithHeadersAndSchemaKey(TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, headers, partition, JSON_KEY);
	
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = IllegalArgumentException.class)
	public void testCreateAvroRecordUrlNotFoundFailure() throws Exception {
		PowerMockito.mockStatic(KafkaPropertiesReader.class);
		Map<String, Properties> read = PowerMockito.mock(HashMap.class);
		PowerMockito.when(KafkaPropertiesReader.read()).thenReturn(read);
		Properties property=PowerMockito.mock(Properties.class);
		PowerMockito.when(read.get(Mockito.anyString())).thenReturn(property);
		PowerMockito.when(read.containsKey(Mockito.anyString())).thenReturn(true);
		AvroProducerRecordUtils
		.createAvroRecordWithHeadersAndSchemaKey(TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, headers, partition, JSON_KEY);
	
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = IllegalArgumentException.class)
	public void testCreateAvroRecordWithValueSchemaAssociationResourceNotSetFailure() throws Exception {
		PowerMockito.mockStatic(KafkaPropertiesReader.class);
		Map<String, Properties> read = PowerMockito.mock(HashMap.class);
		PowerMockito.when(KafkaPropertiesReader.read()).thenReturn(read);
		Properties property=PowerMockito.mock(Properties.class);
		PowerMockito.when(read.get(Mockito.anyString())).thenReturn(property);
		PowerMockito.when(read.containsKey(Mockito.anyString())).thenReturn(false);
		AvroProducerRecordUtils
		.createAvroRecordWithHeaders(TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, headers, partition, JSON_KEY);
	
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = IllegalArgumentException.class)
	public void testCreateAvroRecordWithHeadersUrlNotFoundFailure() throws Exception {
		PowerMockito.mockStatic(KafkaPropertiesReader.class);
		Map<String, Properties> read = PowerMockito.mock(HashMap.class);
		PowerMockito.when(KafkaPropertiesReader.read()).thenReturn(read);
		Properties property=PowerMockito.mock(Properties.class);
		PowerMockito.when(read.get(Mockito.anyString())).thenReturn(property);
		PowerMockito.when(read.containsKey(Mockito.anyString())).thenReturn(true);
		AvroProducerRecordUtils
		.createAvroRecordWithHeaders(TOPIC_NAME, SCHEMA_NAME_KEY, SCHEMA_NAME_VALUE, JSON, headers, partition, JSON_KEY);
	
	}	
	
}
