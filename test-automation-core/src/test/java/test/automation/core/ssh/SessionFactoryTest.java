package test.automation.core.ssh;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.jcraft.jsch.Session;
import com.test.automation.mock.SSHServerMock;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;
import test.automation.core.ssh.SSHRuntimeException;
import test.automation.core.ssh.SessionFactory;

public class SessionFactoryTest {
	private static Map<String, Properties> propertiesBundles = null;
	private static final String USERNAME = "test";
	private static final String PASSWORD = "test";
	private static final Integer PORT = 1300;
	private static SSHServerMock server;

	@BeforeClass
	public static void setup() {
		System.setProperty("confPath", "src/test/resources");
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);
		propertiesBundles = multiPropertiesLoader.loadAndCheck((d, name) -> name.endsWith("-ssh.properties"),
				"ssh.host");
		server = new SSHServerMock(PORT, PASSWORD, USERNAME);
		try {
			server.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void createSessionTest() {
		Properties properties = propertiesBundles.get("default");
		assertNotNull(SessionFactory.createSession(properties));
	}

	@Test
	public void defaultPropertyStrictHostKeyTest() {
		Properties properties = propertiesBundles.get("default");
		Session session = SessionFactory.createSession(properties);
		String configStrictHostKeyChecking = session.getConfig("StrictHostKeyChecking");
		assertEquals("no", configStrictHostKeyChecking);
	}

	@Test
	public void defaultPreferredAuthenticationsTest() {
		Properties properties = propertiesBundles.get("default");
		Session session = SessionFactory.createSession(properties);
		String configStrictHostKeyChecking = session.getConfig("PreferredAuthentications");
		assertEquals("publickey,keyboard-interactive,password", configStrictHostKeyChecking);
	}

	@Test
	public void sshConfigPropertiesTest() {
		Properties properties = propertiesBundles.get("moreconfig");
		Session session = SessionFactory.createSession(properties);
		String configStrictHostKeyChecking = session.getConfig("MaxAuthTries");
		assertEquals("1", configStrictHostKeyChecking);
	}

	@Test
	public void sshAuthenticationKeyTest() {
		Properties properties = propertiesBundles.get("keyAuthorization");
		Session session = SessionFactory.createSession(properties);
		assertNotNull(session);
	}

	@Test(expected = SSHRuntimeException.class)
	public void sshNoPrivateKey() {
		Properties properties = new Properties();
		properties.put("ssh.host", "127.0.0.1");
		properties.put("ssh.port", "66");
		properties.put("ssh.username", "test");
		properties.put("ssh.private.key.path", StringUtils.EMPTY);
		properties.put("ssh.passphrase", "test1234");
		Session session = SessionFactory.createSession(properties);
	}

	@Test(expected = SSHRuntimeException.class)
	public void sshErrPath() {
		Properties properties = new Properties();
		properties.put("ssh.host", "127.0.0.1");
		properties.put("ssh.port", "1300");
		properties.put("ssh.username", "test");
		properties.put("ssh.private.key.path", "fileNotExists");
		properties.put("ssh.passphrase", "test1234");
		Session session = SessionFactory.createSession(properties);
	}

	@AfterClass
	public static void tearDown() {
		try {
			server.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
