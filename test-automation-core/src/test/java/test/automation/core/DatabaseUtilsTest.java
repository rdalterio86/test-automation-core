package test.automation.core;

import static org.junit.Assert.assertNotNull;
import static test.automation.core.DatabaseUtils.db;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;


public class DatabaseUtilsTest {

	@Before
	public void setup(){
		
		System.setProperty("confPath", "src/test/resources");
	}
	@Test
	public void testGetDatabaseTemplate() {
		assertNotNull(db().template());
		
	}

	@Test
	public void testGetRequestTemplateByserverId() {
		assertNotNull(db().template("dbserver"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetRequestTemplateByserverIdKO() {
		assertNotNull(db().template("database2"));
	}
	
	@Test
	public void testGetRequestTemplateBasicAuthenticationConfigured() {
		JdbcTemplate jdbcTemplate = db().template();
		assertNotNull(jdbcTemplate);
	}
}
