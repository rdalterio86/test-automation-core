package test.automation.core;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.test.automation.mock.SSHServerMock;

import test.automation.core.SSHUtils;
import test.automation.core.ssh.SSHIllegalArgumentException;

public class SSHUtilsTest {
	private static final String USERNAME = "test";
	private static final String PASSWORD = "test";
	private static final Integer PORT = 1300;
	private static SSHServerMock server;

	@BeforeClass
	public static void setup() {
		System.setProperty("confPath", "src/test/resources");
		/* setup server */
		server = new SSHServerMock(PORT, PASSWORD, USERNAME);
		try {
			server.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void testSshUtilsTemplate() {
		assertNotNull(SSHUtils.ssh().template());
	}

	@Test
	public void testSShUtilsCustomTemplate() {
		assertNotNull(SSHUtils.ssh().template("moreconfig"));
	}

	@Test(expected = SSHIllegalArgumentException.class)
	public void testSShUtilsNoExistingTemplate() {
		SSHUtils.ssh().template("ssh");
	}

	@AfterClass
	public static void tearDown() {
		try {
			server.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
