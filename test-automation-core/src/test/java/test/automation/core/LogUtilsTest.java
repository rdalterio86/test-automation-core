package test.automation.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static test.automation.core.LogUtils.log;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;

public class LogUtilsTest {

	@Before
	public void setup() {
		System.setProperty("confPath", "src/test/resources");
	}

	@Test
	public void testGetLogger() {
		assertNotNull(log());
	}

	@Test
	public void testGetLoggerClass() {
		assertNotNull(log(LogUtilsTest.class));
	}

	@Test
	public void testLevelInfoEnabled() {
		assertTrue(log().isInfoEnabled());
	}

	@Test
	public void testLevelDebugNotEnabled() {
		assertFalse(log().isDebugEnabled());
	}

	@Test
	public void testLevelErrorEnabled() {
		assertTrue(log(LogUtilsTest.class).isErrorEnabled());
	}

	@Test
	public void testLevelTraceNotEnabled() {
		assertFalse(log(LogUtilsTest.class).isTraceEnabled());
	}

	@Test
	public void testLoggerName() {
		Logger logger = log("testLogger");
		assertTrue(StringUtils.equals(logger.getName(), "testLogger"));

	}
}
