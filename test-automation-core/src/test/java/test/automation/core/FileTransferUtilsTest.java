package test.automation.core;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.test.automation.mock.SSHServerMock;

import test.automation.core.FileTransferUtils;



public class FileTransferUtilsTest {
	private static final String USERNAME = "testSftp";
	private static final String PASSWORD = "testSftp";
	private static final Integer PORT = 1301;
	private static SSHServerMock server;

	@BeforeClass
	public static void setup() throws Exception {
		System.setProperty("confPath", "src/test/resources");
		server = new SSHServerMock(PORT, USERNAME, PASSWORD);
		try {
			server.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void sshFileTransferFactoryTest() {
		assertNotNull(FileTransferUtils.sftp());
	}

	@AfterClass
	public static void cleanup() throws InterruptedException {
		try {
			server.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
