package test.automation.core;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import test.automation.core.TestDataUtils;

public class DataUtilsTest {

	@Test(expected = IllegalArgumentException.class)
	public void testEmptyParameters() throws Exception {

		Map<String, String> parameters = new HashMap<String, String>();
		TestDataUtils.filter(parameters, "createCustomerParametric.json");

	}

	@Test(expected = IllegalArgumentException.class)
	public void testNoData() {

		TestDataUtils.asString("src/test/resources/createCustomer.json");
	}

	@Test
	public void testFilterParameters() {

		Map<String, String> parameters = new HashMap<String, String>();

		parameters.put("companyCode", "testCompanyCode");
		parameters.put("documentationLanguage", "testDocumentationLanguage");
		parameters.put("nationalCode", "testNationalCode");
		parameters.put("companyName", "testCompanyName");
		parameters.put("trustName", "testTrustName");

		String requestFiltered = null;
		try {
			requestFiltered = TestDataUtils.filter(parameters, "createCustomerParametric.json");
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		String requestAsString = TestDataUtils.asString("createCustomer.json");

		Assert.assertTrue(StringUtils.equals(requestFiltered, requestAsString));

	}

}
