package test.automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import com.zaxxer.hikari.HikariDataSource;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

/**
 * Wrap spring jdbc test utils class such as JdbcTestUtils, ScriptUtils, JDBC
 * template
 */
public class JdbcDatabaseUtils {

	private static JdbcDatabaseUtils instance;
	private Map<String, JdbcTemplate> context;

	private static final String DEFAULT_API_PROPERTIES_BUNDLE_KEY = "default";

	private static final String JDBC_URL_PROPERTY = "jdbc.url";
	private static final String JDBC_DATASOURCECLASSNAME_PROPERTY = "jdbc.dataSourceClassName";
	private static final String JDBC_USERNAME = "jdbc.username";
	private static final String JDBC_PASSWORD = "jdbc.password";
	private static final String JDBC_DRIVERCLASSNAME_PROPERTY = "jdbc.driverClassName";

	private JdbcDatabaseUtils() {
		context = new HashMap<String, JdbcTemplate>();
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);

		Map<String, Properties> propertiesBundles = multiPropertiesLoader
				.loadAndCheck((d, name) -> name.endsWith("-database.properties"), JDBC_URL_PROPERTY, JDBC_USERNAME);

		if (!propertiesBundles.containsKey(DEFAULT_API_PROPERTIES_BUNDLE_KEY))
			throw new IllegalArgumentException("No default-api.properties found in the "
					+ ((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath());

		for (Entry<String, Properties> propertyBundle : propertiesBundles.entrySet()) {
			registerDatabaseTemplate(propertyBundle.getKey(), propertyBundle.getValue());

		}

	}

	/**
	 * singleton factory method
	 * 
	 * @return single instance of JdbcDatabaseUtils
	 */
	public static JdbcDatabaseUtils getInstance() {
		if (instance != null) {
			return instance;
		}

		synchronized (JdbcDatabaseUtils.class) {
			if (instance != null) {
				return instance;
			}

			instance = new JdbcDatabaseUtils();
			return instance;

		}
	}

	public JdbcTemplate template() {
		return context.get(DEFAULT_API_PROPERTIES_BUNDLE_KEY);
	}

	public JdbcTemplate template(String resourceName) {
		if (!context.containsKey(resourceName))
			throw new IllegalArgumentException("resource " + resourceName + " is not set");

		return context.get(resourceName);
	}

	private void registerDatabaseTemplate(String resourceName, Properties properties) {
		if (context.containsKey(resourceName))
			throw new IllegalArgumentException(
					"resource " + resourceName + " already set in the context. Override is not allowed");

		// We introduced jdbc.driverClassName property since HiveDataSource
		// class doesn't support properties set for
		// HikariDataSource: url,user and password
		if ((StringUtils.isBlank(properties.getProperty(JDBC_DATASOURCECLASSNAME_PROPERTY)))
				&& (StringUtils.isBlank(properties.getProperty(JDBC_DRIVERCLASSNAME_PROPERTY)))) {

			throw new IllegalArgumentException("Must specify jdbc.driverClassName or jdbc.dataSourceClassName for resource " + resourceName);
		}

		if ((StringUtils.isNotBlank(properties.getProperty(JDBC_DATASOURCECLASSNAME_PROPERTY)))
				&& (StringUtils.isNotBlank(properties.getProperty(JDBC_DRIVERCLASSNAME_PROPERTY)))) {

			throw new IllegalArgumentException(
					"Cannot use jdbc.driverClassName and jdbc.dataSourceClassName together for resource " + resourceName);
		}

		HikariDataSource dataSource = new HikariDataSource();

		if (StringUtils.isNotBlank(properties.getProperty(JDBC_DATASOURCECLASSNAME_PROPERTY))) {

			dataSource.setDataSourceClassName(properties.getProperty(JDBC_DATASOURCECLASSNAME_PROPERTY));

			dataSource.addDataSourceProperty("url", properties.getProperty(JDBC_URL_PROPERTY));
			dataSource.addDataSourceProperty("user", properties.getProperty(JDBC_USERNAME));
			dataSource.addDataSourceProperty("password", properties.getProperty(JDBC_PASSWORD));

		}

		if (StringUtils.isNotBlank(properties.getProperty(JDBC_DRIVERCLASSNAME_PROPERTY))) {

			dataSource.setDriverClassName(properties.getProperty(JDBC_DRIVERCLASSNAME_PROPERTY));
			dataSource.setJdbcUrl(properties.getProperty(JDBC_URL_PROPERTY));
			dataSource.setUsername(properties.getProperty(JDBC_USERNAME));
			dataSource.setPassword(properties.getProperty(JDBC_PASSWORD));

		}

		dataSource.setMaximumPoolSize(20);
		dataSource.setMaxLifetime(30000);
		dataSource.setIdleTimeout(30000);

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		context.put(resourceName, jdbcTemplate);

	}

}
