package test.automation.core.hook.common;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

import cucumber.api.Scenario;
import test.automation.core.AutomationThreadLocal;
import test.automation.core.LogUtils;
import test.automation.core.benchmark.BenchmarkableContext;

public abstract class BenchmarkableBeforeHook {
	
	private static String ANNOTATION = "@benchmarkable";
	private static Logger log = LogUtils.log();
    
	public void runHook(Scenario scenario){
		//check if the monitoring is enabled
		AutomationThreadLocal.unset();
		String monitoring = System.getProperty("monitoring");
		boolean monitoringEnabled = Boolean.parseBoolean(monitoring);
		if (!monitoringEnabled){
			// skip hook, no monitoring needed
			log.info("Monitoring not enabled, skipping benchmarkable annotation processing for scenario " + scenario.getName());
			return;
		}
		
		
		Collection<String> sourceTagNames = scenario.getSourceTagNames();
		sourceTagNames.stream().forEach(s->log.info("Found annotation: " + s));
		//find for 'benchmarkable' annotation
		List<String> result = sourceTagNames
			.stream()
			.filter(s->StringUtils.startsWith(s, ANNOTATION))
			.collect(Collectors.toList());	
		
		String testRunId = System.getProperty("testrunid"); //look for testrunid properties injected from outside
		
		if (result.size() > 0 && StringUtils.isNotBlank(testRunId)){ //build a BanchMarkableContext
			log.info("TestRunId: " +testRunId);
			BenchmarkableContext ctx = buildBenchmarkableContext(result,testRunId);
			AutomationThreadLocal.set(ctx);
		}else{
			log.info("No Banchmarkable annotations found or 'testrunid' properties not provided! Skipping monitoring for scenario " +  scenario.getName());
		}
		
	}

	protected abstract BenchmarkableContext buildBenchmarkableContext(List<String> annotations, String testRunId);

}
