package test.automation.core.hook.ui;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.java.Before;
import test.automation.core.har.HarFileUtils;

public class StoreHarFileHook {

	private static final Logger LOGGER = LoggerFactory.getLogger(StoreHarFileHook.class);
	private static boolean dunit = false;

	/*
	 * A static Boolean flag can indicate when the @Before hook has run more
	 * than once because it isn't "reset" when a new scenario re-instantiates
	 * the step definition classes. The runtime shutdown hook will be called
	 * once all tests are done and the program exits. (Note that a static flag
	 * cannot be used in an @After hook due to the halting problem.)
	 */
	@Before(order = 3)
	public static void beforeAll() {
		if (!dunit) {
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {

					boolean isHarFile = HarFileUtils.isHarFile();

					if (isHarFile) {
						try {
							// if har.file.enabled property is set to true
							// generates HAR file
							// once all tests are done
							HarFileUtils.createHarFile();
						} catch (IOException e) {
							LOGGER.error(e.getMessage());
						}
					}

				}
			});

			dunit = true;
		}
	}

}
