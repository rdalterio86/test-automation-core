package test.automation.core;

import test.automation.core.filetransfer.sftp.SSHFileTransferFactory;

public class FileTransferUtils {
	/**
	 * 
	 * @return single instance of SSHFileTransferFactory
	 */
	public static SSHFileTransferFactory sftp() {
		return SSHFileTransferFactory.getInstance();
	}
}
