package test.automation.core;

import test.automation.core.benchmark.BenchmarkableContext;

public class AutomationThreadLocal {
	public static final ThreadLocal<BenchmarkableContext> automationThreadLocal = new ThreadLocal<>();

	public static void set(BenchmarkableContext context) {
		automationThreadLocal.set(context);
	}

	public static void unset() {
		automationThreadLocal.remove();
	}

	public static BenchmarkableContext get() {
		return automationThreadLocal.get();
	}
}
