package test.automation.core.kafka;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.SslConfigs;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;

/**
 * This class defines configuration for Avro serialization needed to create
 * Kafka Producer Factory
 * 
 */
public class AvroSerializer<K, V> extends KafkaSerializer<K, V> {

	private static final String KAFKA_SSL_ENABLED = "kafka.ssl.enabled";
	private final static String KAFKA_PRODUCER_SSL_PROPERTIES_PATH = "kafka.producer.ssl.properties.path";

	@Override
	public Map<String, Object> producerConfigs(Properties properties) {

		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
				properties.getProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG));
		props.put(KafkaConstants.SCHEMA_REGISTRY_URL, properties.getProperty(KafkaConstants.SCHEMA_REGISTRY_URL));
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				properties.getProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG));
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				properties.getProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG));
		String maxSchemasPerSubjectConfig = properties
				.getProperty(AbstractKafkaAvroSerDeConfig.MAX_SCHEMAS_PER_SUBJECT_CONFIG);
		if (maxSchemasPerSubjectConfig != null) {
			props.put(AbstractKafkaAvroSerDeConfig.MAX_SCHEMAS_PER_SUBJECT_CONFIG, maxSchemasPerSubjectConfig);
		}

		Boolean isKafkaSsl = Boolean.valueOf(properties.getProperty(KAFKA_SSL_ENABLED));
		String pathSslProperties = properties.getProperty(KAFKA_PRODUCER_SSL_PROPERTIES_PATH);

		// If kafka SSL is enabled, configure properties for SSL Encryption and
		// SSL Authentication
		if (isKafkaSsl) {

			// Load properties for SSL Encryption and SSL Authentication from
			// specified properties file
			Properties sslProperties = loadPropertiesFromFile(pathSslProperties);

			// Configure the following two mandatory settings for SSL Encryption
			props.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SSL");
			props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
					extractMandatorySSLPropertyValue(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, sslProperties));

			// Configure the following three mandatory settings for SSL
			// Authentication
			props.put(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG,
					extractMandatorySSLPropertyValue(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG, sslProperties));
			props.put(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG,
					extractMandatorySSLPropertyValue(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG, sslProperties));
			props.put(SslConfigs.SSL_KEY_PASSWORD_CONFIG,
					extractMandatorySSLPropertyValue(SslConfigs.SSL_KEY_PASSWORD_CONFIG, sslProperties));

			// Configure optional settings for SSL Encryption and SSL
			// Authentication.
			// Note that trust store file password is optional since this
			// password is only used to check the integrity of the trust store
			// file. It is not required even if 'trustStoreFile' is non-null.
			Set<String> propertyNames = sslProperties.stringPropertyNames();
			for (String property : propertyNames) {
				// To reduce the number of conditional operators (maximum
				// allowed 3), the expression is splitted in two if statement
				if ((!property.equalsIgnoreCase(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG))
						&& (!property.equalsIgnoreCase(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG))) {
					if ((!property.equalsIgnoreCase(SslConfigs.SSL_KEYSTORE_LOCATION_CONFIG))
							&& (!property.equalsIgnoreCase(SslConfigs.SSL_KEYSTORE_PASSWORD_CONFIG))
							&& (!property.equalsIgnoreCase(SslConfigs.SSL_KEY_PASSWORD_CONFIG))) {
						props.put(property, sslProperties.getProperty(property));
					}
				}
			}
		}

		return props;
	}

	private Properties loadPropertiesFromFile(String filePath) {

		try {

			if (StringUtils.isBlank(filePath)) {
				throw new IllegalArgumentException(
						"Unable to find property kafka.producer.ssl.properties.path to configure Kafka producer for SSL!");
			}

			File propertiesFile = new File(filePath);
			Properties p = new Properties();
			p.load(new FileInputStream(propertiesFile));
			return p;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	private String extractMandatorySSLPropertyValue(String propertyKey, Properties dictionary) {
		if (dictionary == null) {
			throw new IllegalArgumentException("Property dictionary null!");
		}
		if (StringUtils.isBlank(propertyKey)) {
			throw new IllegalArgumentException("Property key is blank!");
		}
		String property = dictionary.getProperty(propertyKey);
		if (StringUtils.isBlank(property)) {
			throw new IllegalArgumentException("Unable to find property '" + propertyKey
					+ "' to enable encryption and authentication with SSL for Kafka producer. It is mandatory!");
		}
		return property;

	}

}
