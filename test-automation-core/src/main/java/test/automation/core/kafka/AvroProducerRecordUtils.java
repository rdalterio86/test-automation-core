package test.automation.core.kafka;

import static io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig.MAX_SCHEMAS_PER_SUBJECT_CONFIG;
import static io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig.MAX_SCHEMAS_PER_SUBJECT_DEFAULT;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.SchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;

/**
 * This class defines utility methods for record production using Avro
 * serialization
 * 
 *
 */

public class AvroProducerRecordUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(AvroProducerRecordUtils.class);

	private static Map<String, SchemaRegistryClient> schemaRegistryClients = new HashMap<>();
	
	private static Map<String,Integer> schemaCachedMap = new HashMap<>(); 
	
	private AvroProducerRecordUtils() {
		
	}
	
	
	/**
	   * This method create avro records managing the key as an Object and adding specific headers
	   * 
	   * @param resourceName, resource name for properties file from which to read SCHEMA_REGISTRY_URL
	   * @param topicName, name of Kafka topic
	   * @param schemaName, name of schema to use for value serialization
	   * @param json, json event to publish
	   * @param headers, Kafka record headers
	   * @param partition, Kafka topic partition
	   * @param key, record key as Object
	   * @return the avro record with headers and Object key
	   */
	  public static ProducerRecord<Object, Object> createAvroRecordWithHeaders(
	      String resourceName,
	      String topicName,
	      String schemaName,
	      String json,
	      RecordHeaders headers,
	      Integer partition,
	      Object key)
	      throws IOException,
	      RestClientException {
	    
	    if (!KafkaPropertiesReader.read().containsKey(resourceName))
	      throw new IllegalArgumentException("resource " + resourceName
	          + " is not set");
	    
	    String schemaUrl = KafkaPropertiesReader.read().get(resourceName)
	        .getProperty(KafkaConstants.SCHEMA_REGISTRY_URL);
	    
	    if (StringUtils.isBlank(schemaUrl)) {
	      throw new IllegalArgumentException(
	          "Unable to find property schema.registry.url!");
	    }
	    
	    // Instantiate the schema registry client
	    SchemaRegistryClient schemaClient = getSchemaRegistryClient(schemaUrl, resourceName);
	  
	    int schemaId = 0;
	    /*
	     * Added map to cache a schema id to avoid further api rest calls when a schema is already loaded.
	     * This has been done to improve the load performance tests  
	     */
	    if (!schemaCachedMap.containsKey(schemaName)){
		    // get the schema by id
		     schemaId = schemaClient.getLatestSchemaMetadata(schemaName).getId();
		     schemaCachedMap.put(schemaName, schemaId);
	    } else {
	    	
	    	schemaId = schemaCachedMap.get(schemaName);
	    	
	    }
	    
	    Schema schema = schemaClient.getById(schemaId);
	    
	    // Get the json decoder, this is used to parse a json and to get the
	    // record
	    Decoder decoder = DecoderFactory.get().jsonDecoder(schema, json);
	    DatumReader<GenericData.Record> reader = new GenericDatumReader<>(
	        schema);
	    GenericRecord datum = reader.read(null, decoder);
	    
	    // Instantiate the record
	    return new ProducerRecord<Object, Object>(topicName, partition, key,
	        datum, headers);
	    
	  }
	  
	  private static SchemaRegistryClient getSchemaRegistryClient(String schemaUrl, String resourceName) {
	    if (schemaRegistryClients.containsKey(schemaUrl)) {
	      return schemaRegistryClients.get(schemaUrl);
	    }
	    
	    synchronized (AvroProducerRecordUtils.class) {
	      if (!schemaRegistryClients.containsKey(schemaUrl)) {
	        String maxSchemasPerSubject = KafkaPropertiesReader.read().get(resourceName)
	            .getProperty(MAX_SCHEMAS_PER_SUBJECT_CONFIG);
	        int maxSchemas;
	        try {
	          maxSchemas =
	              maxSchemasPerSubject == null ? MAX_SCHEMAS_PER_SUBJECT_DEFAULT : Integer.valueOf(maxSchemasPerSubject);
	        } catch (Exception e) {
	          maxSchemas = MAX_SCHEMAS_PER_SUBJECT_DEFAULT;
	          if (LOGGER.isWarnEnabled()) {
	            LOGGER.warn("Unable to set the property [{}] to [{}], so it has been set to the default value [{}].",
	                MAX_SCHEMAS_PER_SUBJECT_CONFIG, maxSchemasPerSubject, MAX_SCHEMAS_PER_SUBJECT_DEFAULT);
	          }
	        }
	        SchemaRegistryClient schemaRegistryClient =
	            new CachedSchemaRegistryClient(schemaUrl, maxSchemas);
	        schemaRegistryClients.put(schemaUrl, schemaRegistryClient);
	      }
	      
	      return schemaRegistryClients.get(schemaUrl);
	    }
	  }
	  
	  /**
	   * This method create avro records managing the key as an Object and adding specific headers using default
	   * properties file for SCHEMA_REGISTRY_URL reading
	   * 
	   * @param topicName, name of Kafka topic
	   * @param schemaName, name of schema to use for value serialization
	   * @param json, json event to publish
	   * @param headers, Kafka record headers
	   * @param partition, Kafka topic partition
	   * @param key, record key as an Object
	   * @return the avro record with headers and key
	   */
	  public static ProducerRecord<Object, Object> createAvroRecordWithHeaders(
	      String topicName,
	      String schemaName,
	      String json,
	      RecordHeaders headers,
	      Integer partition,
	      Object key)
	      throws IOException,
	      RestClientException {
	    
	    return createAvroRecordWithHeaders(KafkaConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY, topicName, schemaName,
	        json, headers, partition, key);
	    
	  }
	  
	  /**
	   * This method create avro records without headers and key
	   * 
	   * @param resourceName, resource name for properties file from which to read SCHEMA_REGISTRY_URL
	   * @param topicName, name of Kafka topic
	   * @param schemaName, name of schema to use for value serialization
	   * @param json, json event to publish
	   * @return the avro record without headers and key
	   */
	  public static ProducerRecord<Object, Object> createAvroRecord(
	      String resourceName,
	      String topicName,
	      String schemaName,
	      String json)
	      throws IOException,
	      RestClientException {
	    
	    return createAvroRecordWithHeaders(resourceName, topicName, schemaName,
	        json, null, null, null);
	    
	  }
	  
	  /**
	   * This method create avro records without headers and key using default properties
	   * file for SCHEMA_REGISTRY_URL reading
	   * 
	   * @param topicName, name of Kafka topic
	   * @param schemaName, name of schema to use for value serialization
	   * @param json, json event to publish
	   * @return the avro record without headers and key
	   */
	  public static ProducerRecord<Object, Object> createAvroRecord(
	      String topicName,
	      String schemaName,
	      String json)
	      throws IOException,
	      RestClientException {
	    
	    return createAvroRecordWithHeaders(KafkaConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY, topicName, schemaName,
	        json, null, null, null);
	    
	  }
	  
	  /**
	   * This method create avro records with Object key and without headers
	   * 
	   * @param resourceName, resource name for properties file from which to read SCHEMA_REGISTRY_URL
	   * @param topicName, name of Kafka topic
	   * @param schemaName, name of schema to use for value serialization
	   * @param json, json event to publish
	   * @param partition, Kafka topic partition
	   * @param key, record key as an Object
	   * @return the avro record with Object key without headers
	   */
	  public static ProducerRecord<Object, Object> createAvroRecord(
	      String resourceName,
	      String topicName,
	      String schemaName,
	      String json,
	      Integer partition,
	      Object key)
	      throws IOException,
	      RestClientException {
	    
	    return createAvroRecordWithHeaders(resourceName, topicName, schemaName,
	        json, null, partition, key);
	    
	  }
	  
	  /**
	   * This method create avro records with Object key and without headers using default
	   * properties file for SCHEMA_REGISTRY_URL reading
	   * 
	   * @param topicName, name of Kafka topic
	   * @param schemaName, name of schema to use for value serialization
	   * @param json, json event to publish
	   * @param partition, Kafka topic partition
	   * @param key, record key as an Object
	   * @return the avro record with Object key without headers
	   */
	  public static ProducerRecord<Object, Object> createAvroRecord(
	      String topicName,
	      String schemaName,
	      String json,
	      Integer partition,
	      Object key)
	      throws IOException,
	      RestClientException {
	    
	    return createAvroRecordWithHeaders(KafkaConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY, topicName, schemaName,
	        json, null, partition, key);
	  }
	
	/**
	 * This method create avro records with avro event key adding specific headers
	 * 
	 * @param resourceName,
	 *            resource name for properties file from which to read
	 *            SCHEMA_REGISTRY_URL
	 * @param topicName,
	 *            name of Kafka topic
	 * @param schemaNameKey,
	 *            name of schema to use for event key serialization
	 * @param schemaNameValue,
	 *            name of schema to use for event value serialization
	 * @param jsonValue,
	 *            json for the event value to publish
	 * @param headers,
	 *            Kafka record headers
	 * @param partition,
	 *            Kafka topic partition
	 * @param jsonKey,
	 *            json for the record key
	 * @return the avro record with headers
	 */
	public static ProducerRecord<Object, Object> createAvroRecordWithHeadersAndSchemaKey(String resourceName, String topicName,
			String schemaNameKey, String schemaNameValue, String jsonValue, RecordHeaders headers, Integer partition,
			String jsonKey) throws IOException, RestClientException {

		if (!KafkaPropertiesReader.read().containsKey(resourceName))
			throw new IllegalArgumentException("resource " + resourceName + " is not set");

		String schemaUrl = KafkaPropertiesReader.read().get(resourceName)
				.getProperty(KafkaConstants.SCHEMA_REGISTRY_URL);

		if (StringUtils.isBlank(schemaUrl)) {
			throw new IllegalArgumentException("Unable to find property schema.registry.url!");
		}

		// Instantiate the schema registry client
		SchemaRegistryClient schemaClient = getSchemaRegistryClient(schemaUrl, resourceName);

		// Kafka event key may be null
		GenericRecord datumKey = null;

		// If event key isn't null, it must be formatted as an Avro subject in
		// the same way as the event value
		if ((!StringUtils.isBlank(schemaNameKey)) && (!StringUtils.isBlank(jsonKey))) {
			
		    int schemaKeyId = 0;
		    /*
		     * Added map to cache a schema id to avoid further api rest calls when a schema is already loaded.
		     * This has been done to improve the load performance tests  
		     */
		    if (!schemaCachedMap.containsKey(schemaNameKey)){
			    // get the schema by id
		    	schemaKeyId = schemaClient.getLatestSchemaMetadata(schemaNameKey).getId();
			     schemaCachedMap.put(schemaNameKey, schemaKeyId);
		    } else {
		    	
		    	schemaKeyId = schemaCachedMap.get(schemaNameKey);
		    	
		    }

			Schema schemaKey = schemaClient.getById(schemaKeyId);

			// Get the json decoder for the event key, this is used to parse the
			// json for the event key and to get the
			// record
			Decoder decoderKey = DecoderFactory.get().jsonDecoder(schemaKey, jsonKey);
			DatumReader<GenericData.Record> readerKey = new GenericDatumReader<>(schemaKey);
			datumKey = readerKey.read(null, decoderKey);

		}

		// get the schema of event value by id
	    int schemaValueId = 0;
	    /*
	     * Added map to cache a schema id to avoid further api rest calls when a schema is already loaded.
	     * This has been done to improve the load performance tests  
	     */
	    if (!schemaCachedMap.containsKey(schemaNameValue)){
		    // get the schema by id
	    	schemaValueId = schemaClient.getLatestSchemaMetadata(schemaNameValue).getId();
		     schemaCachedMap.put(schemaNameValue, schemaValueId);
	    } else {
	    	
	    	schemaValueId = schemaCachedMap.get(schemaNameValue);
	    	
	    }
		
		Schema schemaValue = schemaClient.getById(schemaValueId);

		// Get the json decoder for the event value, this is used to parse the
		// json for the event value and to get the
		// record
		Decoder decoderValue = DecoderFactory.get().jsonDecoder(schemaValue, jsonValue);
		DatumReader<GenericData.Record> readerValue = new GenericDatumReader<>(schemaValue);
		GenericRecord datumValue = readerValue.read(null, decoderValue);

		// Instantiate the record
		return new ProducerRecord<Object, Object>(topicName, partition, datumKey, datumValue, headers);

	}

	/**
	 * This method create avro records with specific headers and avro event key using default
	 * properties file for SCHEMA_REGISTRY_URL reading
	 * 
	 * @param topicName,
	 *            name of Kafka topic
	 * @param schemaNameKey,
	 *            name of schema to use for event key serialization
	 * @param schemaNameValue,
	 *            name of schema to use for event value serialization
	 * @param jsonValue,
	 *            json for the event value to publish
	 * @param headers,
	 *            Kafka record headers
	 * @param partition,
	 *            Kafka topic partition
	 * @param jsonKey,
	 *            json for the record key
	 * @return the avro record with headers
	 */
	public static ProducerRecord<Object, Object> createAvroRecordWithHeadersAndSchemaKey(String topicName, String schemaNameKey,
			String schemaNameValue, String jsonValue, RecordHeaders headers, Integer partition, String jsonKey)
			throws IOException, RestClientException {

		return createAvroRecordWithHeadersAndSchemaKey(KafkaConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY, topicName, schemaNameKey, schemaNameValue, jsonValue, headers,
				partition, jsonKey);

	}

	/**
	 * This method create avro records using avro event key without headers
	 * 
	 * @param resourceName,
	 *            resource name for properties file from which to read
	 *            SCHEMA_REGISTRY_URL
	 * @param topicName,
	 *            name of Kafka topic
	 * @param schemaNameKey,
	 *            name of schema to use for event key serialization
	 * @param schemaNameValue,
	 *            name of schema to use for event value serialization
	 * @param jsonValue,
	 *            json for the event value to publish
	 * @param partition,
	 *            Kafka topic partition
	 * @param jsonKey,
	 *            json for the record key
	 * @return the avro record with avro key without headers
	 */
	public static ProducerRecord<Object, Object> createAvroRecordWithSchemaKey(String resourceName, String topicName,
			String schemaNameKey, String schemaNameValue, String jsonValue, Integer partition, String jsonKey)
			throws IOException, RestClientException {

		return createAvroRecordWithHeadersAndSchemaKey(resourceName, topicName, schemaNameKey, schemaNameValue, jsonValue, null,
				partition, jsonKey);

	}

	/**
	 * This method create avro records with avro key without headers using default
	 * properties file for SCHEMA_REGISTRY_URL reading
	 * 
	 * @param topicName,
	 *            name of Kafka topic
	 * @param schemaNameKey,
	 *            name of schema to use for event key serialization
	 * @param schemaNameValue,
	 *            name of schema to use for event value serialization
	 * @param jsonValue,
	 *            json for the event value to publish
	 * @param partition,
	 *            Kafka topic partition
	 * @param jsonKey,
	 *            json for the record key
	 * @return the avro record with avro key without headers
	 */
	public static ProducerRecord<Object, Object> createAvroRecordWithSchemaKey(String topicName, String schemaNameKey,
			String schemaNameValue, String jsonValue, Integer partition, String jsonKey)
			throws IOException, RestClientException {

		return createAvroRecordWithHeadersAndSchemaKey(KafkaConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY, topicName, schemaNameKey, schemaNameValue, jsonValue, null,
				partition, jsonKey);
	}

	/**
	 * This method create Kafka headers for Avro records
	 * 
	 * @param headerMap,
	 *            map containing header key and header value
	 * 
	 * @return the headers for avro records
	 */
	public static RecordHeaders createRecordHeaders(Map<String, String> headerMap) {
		RecordHeaders headers = null;
		if (headerMap != null) {
			headers = new RecordHeaders();
			for (Entry<String, String> header : headerMap.entrySet()) {
				headers.add(new RecordHeader(header.getKey(), header.getValue().getBytes(StandardCharsets.UTF_8)));
			}
		}
		return headers;
	}
}
