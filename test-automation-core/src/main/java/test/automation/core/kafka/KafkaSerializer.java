package test.automation.core.kafka;

import java.util.Map;
import java.util.Properties;

import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;

/**
 * A message in Kafka is a key-value pair with a small amount of associated
 * metadata. As Kafka stores and transports Byte arrays, we need to specify the
 * format from which the key and value will be serialized.
 * 
 * This abstract class exposes defined templates to create ProducerFactory<K, V>
 * needed to instantiate KafkaTemplate<K, V>.
 * 
 */
public abstract class KafkaSerializer<K, V> {

	public abstract Map<String, Object> producerConfigs(Properties properties);

	public final ProducerFactory<K, V> producerFactory(Properties properties) {

		return new DefaultKafkaProducerFactory<>(producerConfigs(properties));

	}

}
