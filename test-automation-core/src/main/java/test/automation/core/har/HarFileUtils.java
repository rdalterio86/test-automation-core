package test.automation.core.har;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.proxy.CaptureType;
import test.automation.core.UIUtils;

public class HarFileUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(HarFileUtils.class);

	// har file properties specific
	private static final String HAR_FILE_ENABLED = "har.file.enabled";
	private static final String HAR_FILE_PROXY_PORT = "har.file.proxy.port";
	private static final String HAR_FILE_PATH = "har.file.path";
	private static final String HAR_FILE_LABEL = "har.file.label";
	private static final String HAR_FILE_CAPTURE_TYPES = "har.file.capture.types";
	//Fix Sonar critical issue
	private static final String HAR_STRING = "HAR file ";

	private static BrowserMobProxy proxy = null;

	private static String harFileLabel = null;

	private HarFileUtils() {

	}

	private static void setUpBrowserMobProxy(Properties p) {

		if (proxy == null) {

			proxy = new BrowserMobProxyServer();
		}

		if (!proxy.isStarted()) {

			// All upstream servers will be trusted
			proxy.setTrustAllServers(true);
			
			// start the MobProxyServer for generating HAR files
			String harFileProxyPort = p.getProperty(HAR_FILE_PROXY_PORT);
			if (StringUtils.isNotBlank(harFileProxyPort)) {
				LOGGER.info("har.file.proxy.port set. Har file proxy started on port " + harFileProxyPort);
				proxy.start(Integer.valueOf(harFileProxyPort));
			} else {
				proxy.start();
			}

			// according to capture types set in the property
			// har.file.capture.types
			// we need to configure BrowserMobProxy in order to define HAR file
			// details
			String captureTypesString = p.getProperty(HAR_FILE_CAPTURE_TYPES);

			if (StringUtils.isNotBlank(captureTypesString)) {

				String[] captureTypesStringArray = captureTypesString.split(",");
				List<String> captureTypesStringList = Arrays.asList(captureTypesStringArray);

				List<CaptureType> captureTypesList = new ArrayList<>();

				for (String captureTypeString : captureTypesStringList) {
					if (captureTypeString.equalsIgnoreCase("request_headers")) {
						captureTypesList.add(CaptureType.REQUEST_HEADERS);
					} else if (captureTypeString.equalsIgnoreCase("request_cookies")) {
						captureTypesList.add(CaptureType.REQUEST_COOKIES);
					} else if (captureTypeString.equalsIgnoreCase("request_content")) {
						captureTypesList.add(CaptureType.REQUEST_CONTENT);
					} else if (captureTypeString.equalsIgnoreCase("request_binary_content")) {
						captureTypesList.add(CaptureType.REQUEST_BINARY_CONTENT);
					} else if (captureTypeString.equalsIgnoreCase("response_headers")) {
						captureTypesList.add(CaptureType.RESPONSE_HEADERS);
					} else if (captureTypeString.equalsIgnoreCase("response_cookies")) {
						captureTypesList.add(CaptureType.RESPONSE_COOKIES);
					} else if (captureTypeString.equalsIgnoreCase("response_content")) {
						captureTypesList.add(CaptureType.RESPONSE_CONTENT);
					} else if (captureTypeString.equalsIgnoreCase("response_binary_content")) {
						captureTypesList.add(CaptureType.RESPONSE_BINARY_CONTENT);
					} else if (captureTypeString.equalsIgnoreCase("all")) {
						captureTypesList.add(CaptureType.REQUEST_BINARY_CONTENT);
						captureTypesList.add(CaptureType.REQUEST_CONTENT);
						captureTypesList.add(CaptureType.REQUEST_COOKIES);
						captureTypesList.add(CaptureType.REQUEST_HEADERS);
						captureTypesList.add(CaptureType.RESPONSE_BINARY_CONTENT);
						captureTypesList.add(CaptureType.RESPONSE_CONTENT);
						captureTypesList.add(CaptureType.RESPONSE_COOKIES);
						captureTypesList.add(CaptureType.RESPONSE_HEADERS);
					} else {
						LOGGER.info("Unsupported capture type: " + captureTypeString
								+ ". Default capture type all will be set!");
						captureTypesList.clear();
						captureTypesList.add(CaptureType.REQUEST_BINARY_CONTENT);
						captureTypesList.add(CaptureType.REQUEST_CONTENT);
						captureTypesList.add(CaptureType.REQUEST_COOKIES);
						captureTypesList.add(CaptureType.REQUEST_HEADERS);
						captureTypesList.add(CaptureType.RESPONSE_BINARY_CONTENT);
						captureTypesList.add(CaptureType.RESPONSE_CONTENT);
						captureTypesList.add(CaptureType.RESPONSE_COOKIES);
						captureTypesList.add(CaptureType.RESPONSE_HEADERS);
					}
				}

				for (CaptureType captureType : captureTypesList) {

					// enable more detailed HAR capture, if desired (see
					// CaptureType
					// for the complete list)
					proxy.enableHarCaptureTypes(captureType);

				}

			} else {

				// default the HAR file is configured with all Capture Types
				proxy.enableHarCaptureTypes(CaptureType.REQUEST_BINARY_CONTENT, CaptureType.REQUEST_CONTENT,
						CaptureType.REQUEST_COOKIES, CaptureType.REQUEST_HEADERS, CaptureType.RESPONSE_BINARY_CONTENT,
						CaptureType.RESPONSE_CONTENT, CaptureType.RESPONSE_COOKIES, CaptureType.RESPONSE_HEADERS);

			}

			harFileLabel = p.getProperty(HAR_FILE_LABEL);

			// BrowserMob stores all the content in HAR file. So we need to
			// enable HAR capture setting
			// the HAR file label specified in the property har.file.label
			if (StringUtils.isNotBlank(harFileLabel)) {
				proxy.newHar(harFileLabel);
			} else {
				proxy.newHar();
			}

		}

	}

	public static Proxy buildSeleniumProxy(Properties p) {

		setUpBrowserMobProxy(p);
		// get the Selenium proxy object
		return ClientUtil.createSeleniumProxy(proxy);

	}

	public static void createHarFile() throws IOException {

		WebDriver currentDriver = UIUtils.ui().getCurrentDriver();

		if (proxy == null) {

			return;
		}

		// get the HAR data
		Har har = proxy.getHar();

		// default file name prefix
		String fileName = "Page0";

		if (StringUtils.isNotBlank(harFileLabel)) {

			fileName = harFileLabel;

		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

		String out_timestamp = sdf.format(new Date());
		String out_filename = fileName + "_" + out_timestamp + ".har";

		String harFilePath = UIUtils.ui().getCurrentProperties().getProperty(HAR_FILE_PATH);

		FileOutputStream harFile = null;

		if (StringUtils.isNotBlank(harFilePath)) {

			if (StringUtils.isBlank(FilenameUtils.getPrefix(harFilePath))) {

				// Write HAR Data in a File using a relative path
				File relativeFile = new File(
						"." + File.separatorChar + harFilePath + File.separatorChar + out_filename);
				harFile = FileUtils.openOutputStream(relativeFile);
				LOGGER.info(HAR_STRING + out_filename + " generated in the directory "
						+ FilenameUtils.getFullPathNoEndSeparator(relativeFile.getCanonicalPath()));

			} else {

				// Write HAR Data in a File using an absolute path
				File absoluteFile = new File(harFilePath + File.separatorChar + out_filename);
				harFile = FileUtils.openOutputStream(absoluteFile);
				LOGGER.info(HAR_STRING + out_filename + " generated in the directory " + absoluteFile.getParent());

			}

		} else {

			// Write HAR Data in a File using default path
			String defaultPath = System.getProperty("java.io.tmpdir") + "automation_har_file_dir";
			File defaultFile = new File(defaultPath + File.separatorChar + out_filename);
			harFile = FileUtils.openOutputStream(defaultFile);
			LOGGER.info(HAR_STRING + out_filename + " generated in default directory " + defaultFile.getParent());
		}

		if (harFile != null) {

			// store the HAR file
			har.writeTo(harFile);
		}

		if (currentDriver != null) {

			proxy.stop();
		}

	}

	public static boolean isHarFile() {
		return Boolean.valueOf(UIUtils.ui().getCurrentProperties().getProperty(HAR_FILE_ENABLED));
	}

}
