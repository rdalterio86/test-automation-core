package test.automation.core;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.BasicConfigurator;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

public class LogUtils {
	private final static String DEFAULT_CONFIGURATION_FILE_NAME = "logback.xml";
	
	static {		
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		
		try {
			// Getting logback.xml absolute path 
			String userDir = System.getProperty("user.dir");
			String confPath = userDir + File.separator + System.getProperty("confPath");
			
			// LOGGER CONFIGURATION WITH JORANCONFIGURATOR
			// With JoranConfigurator we will specify logback.xml configuration file location manually
			JoranConfigurator configurator = new JoranConfigurator();
			configurator.setContext(context);
			context.reset();
			configurator.doConfigure(confPath + File.separator + DEFAULT_CONFIGURATION_FILE_NAME);			
		} catch (JoranException e) {
			// Print configuration errors or warnings from the Logger context
			BasicConfigurator configurator = new BasicConfigurator();
			configurator.setContext(context);
			context.reset();
			configurator.configure(context);
			Logger exceptionLogger = LoggerFactory.getLogger(LogUtils.class);
			exceptionLogger.warn("Configuration file not found. Default configuration will be used");	
		}
	}
	
	public static Logger log(){
		return LoggerFactory.getLogger(getCallerClass());
	}
	
	@SuppressWarnings("rawtypes")
	public static Logger log(Class clazz){
		return LoggerFactory.getLogger(clazz);
	}
	
	public static Logger log(String loggerName){
		return LoggerFactory.getLogger(loggerName);
	}
	
	// This method will get caller class name
	private static String getCallerClass() { 
		StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
		for (StackTraceElement ste : stElements) {
			if (!ste.getClassName().equals(LogUtils.class.getName()) && ste.getClassName().indexOf("java.lang.Thread")!=0) {
				return ste.getClassName();
			}
		}
		return null;
	}
}
