package test.automation.core.properties;

import java.io.FilenameFilter;
import java.util.Map;
import java.util.Properties;

public interface MultiPropertiesLoader {

		
	/*
	 * Load all properties found in the properties. Validate propertiesKeys list as mandatory properties.
	 */
	public Map<String,Properties> loadAndCheck(FilenameFilter filenameFilter,String...propertieskeys);
	
		
}
