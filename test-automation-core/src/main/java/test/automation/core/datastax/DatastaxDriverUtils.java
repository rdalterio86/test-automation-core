package test.automation.core.datastax;

import com.datastax.driver.core.Cluster.Builder;

/**
 * This class provides an utility method to load, at runtime, the exact datastax
 * cluster builder (Community or Enterprise) required to perform the test script
 * 
 *
 */

public class DatastaxDriverUtils {

	public static Builder defineClusterBuilder(String datastaxClassDriver)
			throws InstantiationException, IllegalAccessException, DatastaxDriverNotFoundException {

		Class<?> clusterBuilder = null;

		try {
			clusterBuilder = Class.forName(datastaxClassDriver+"$Builder");
		} catch (ClassNotFoundException e) {
			throw new DatastaxDriverNotFoundException(
					"Not found the Datastax class driver "+datastaxClassDriver+" in the classpath", e);
		}

		return (Builder) clusterBuilder.newInstance();
	}

}
