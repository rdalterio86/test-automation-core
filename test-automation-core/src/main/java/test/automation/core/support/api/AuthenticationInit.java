package test.automation.core.support.api;

import org.apache.commons.lang.StringUtils;

import io.restassured.authentication.BasicAuthScheme;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class AuthenticationInit {

	private AuthenticationInit() {

	}

	public static RequestSpecification addBasicAuthentication(RequestSpecification request, String username,
			String password, Boolean usePreemptive, RequestSpecBuilder requestSpecBuilder) {
		if (!StringUtils.isBlank(username) && !StringUtils.isBlank(password)) {
			if (usePreemptive) {
				request.auth().preemptive().basic(username, password);
			} else {
				BasicAuthScheme basicAuthScheme = new BasicAuthScheme();
				basicAuthScheme.setUserName(username);
				basicAuthScheme.setPassword(password);
				requestSpecBuilder.setAuth(basicAuthScheme);
				request = requestSpecBuilder.build();
			}
		}
		return request;
	}

}
