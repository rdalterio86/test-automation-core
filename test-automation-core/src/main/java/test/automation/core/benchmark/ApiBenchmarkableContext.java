package test.automation.core.benchmark;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class ApiBenchmarkableContext extends BenchmarkableContext{
	
	public ApiBenchmarkableContext(List<String> annotations, String testRunId){
		
		super(annotations,testRunId);
	}		
		
	@Override
	public List<BenchmarkableAnnotationObject> convertAnnotation(
			List<String> annotations) {
        
		List<BenchmarkableAnnotationObject> objectList = new ArrayList<>();
		
		BenchmarkableAnnotationObject obj;
		for (String annotation: annotations) {
			if (StringUtils.isNotBlank(annotation)){
				String payload = StringUtils.substringBetween(annotation, "@benchmarkable(", ")");
				String[] tokens = StringUtils.split(payload, ",");
				obj = new BenchmarkableAnnotationObject();
				for (String token : tokens) {
					String[] split = token.split("=");
					if (split.length==2){
					String key = split[0].trim();
					String value = split[1].trim().replaceAll("^\"|\"$", "");
					if (StringUtils.equals(key, "name")){
						obj.setName(value);
					}else if (StringUtils.equals(key, "path")){
						obj.setPath(value);
					}else if(StringUtils.equals(key, "method")){
						obj.setMethod(value);
					}
				  }	
				}
				
				if(StringUtils.isEmpty(obj.getName())){
					throw new IllegalArgumentException("Missed name property inside benchmarkable annotation"); 
				} else if (StringUtils.isEmpty(obj.getPath())){
					throw new IllegalArgumentException("Missed path property inside benchmarkable annotation"); 
				} else if (StringUtils.isEmpty(obj.getMethod())){
					throw new IllegalArgumentException("Missed method property inside benchmarkable annotation"); 
				}
				
				objectList.add(obj);
				
			  }
		}	
		
		return objectList;
		
	}	
	
}
