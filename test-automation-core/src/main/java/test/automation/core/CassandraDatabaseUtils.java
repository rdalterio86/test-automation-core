package test.automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Cluster.Builder;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.policies.ExponentialReconnectionPolicy;
import com.datastax.driver.core.policies.ReconnectionPolicy;

import test.automation.core.datastax.DatastaxDriverUtils;
import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

/**
 * Wrap com.datastax.driver.core.Session in order to execute queries on
 * Cassandra databases.
 *  
 */
public class CassandraDatabaseUtils {

	private static CassandraDatabaseUtils instance;
	private Map<String, Session> context;

	private static final String DEFAULT_CASSANDRA_PROPERTIES_BUNDLE_KEY = "default";

	private static final String CASSANDRA_HOST = "cassandra.host";
	private static final String CASSANDRA_PORT = "cassandra.port";
	private static final String CASSANDRA_USERNAME = "cassandra.username";
	private static final String CASSANDRA_PASSWORD = "cassandra.password";
	private static final String CASSANDRA_KEYSPACE = "cassandra.keyspace";
	private static final String CASSANDRA_SSL_ENABLED = "cassandra.ssl.enabled";
	private static final String CASSANDRA_SSL_TRUST_STORE = "cassandra.ssl.trustStore";
	private static final String CASSANDRA_SSL_TRUST_STORE_PASSWORD = "cassandra.ssl.trustStorePassword";
	private static final String LOCAL_CORE_CONNECTIONS = "localCoreConnections";
	private static final String LOCAL_MAX_CONNECTIONS = "localMaxConnections";
	private static final String REMOTE_CORE_CONNECTIONS = "remoteCoreConnections";
	private static final String REMOTE_MAX_CONNECTIONS = "remoteMaxConnections";
	private static final String BASE_DELAY_MS = "reconnectionPolicy.baseDelayMs";
	private static final String MAX_DELAY_MS = "reconnectionPolicy.maxDelayMs";
	private static final String DATASTAX_DRIVERCLASSNAME_PROPERTY ="datastax.driverClassName";
	

	private CassandraDatabaseUtils() {
		context = new HashMap<String, Session>();
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);

		Map<String, Properties> propertiesBundles = multiPropertiesLoader
				.loadAndCheck(
						(d, name) -> name.endsWith("-cassandra.properties"),
						CASSANDRA_HOST, CASSANDRA_PORT, CASSANDRA_USERNAME,
						CASSANDRA_KEYSPACE, DATASTAX_DRIVERCLASSNAME_PROPERTY);

		if (!propertiesBundles
				.containsKey(DEFAULT_CASSANDRA_PROPERTIES_BUNDLE_KEY))
			throw new IllegalArgumentException(
					"No default-cassandra.properties found in the "
							+ ((FileMultiPropertiesLoaderImpl) multiPropertiesLoader)
									.getPath());

		for (Entry<String, Properties> propertyBundle : propertiesBundles
				.entrySet()) {
			registerCassandraSession(propertyBundle.getKey(),
					propertyBundle.getValue());

		}

	}

	/**
	 * singleton factory method
	 * 
	 * @return single instance of CassandraDatabaseUtils
	 */
	public static CassandraDatabaseUtils getInstance() {
		if (instance != null) {
			return instance;
		}

		synchronized (CassandraDatabaseUtils.class) {
			if (instance != null) {
				return instance;
			}

			instance = new CassandraDatabaseUtils();
			return instance;

		}
	}

	/**
	 * returning the Java Driver DataStax Session for default Cassandra cluster.
	 * It was always guaranteed to have a Session for default cluster.
	 */
	public Session template() {
		return context.get(DEFAULT_CASSANDRA_PROPERTIES_BUNDLE_KEY);
	}

	public Session template(String resourceName) {
		if (!context.containsKey(resourceName))
			throw new IllegalArgumentException("resource " + resourceName
					+ " is not set");

		return context.get(resourceName);
	}

	/**
	 * Create a com.datastax.driver.core.Session
	 * 
	 * @param resourceName
	 * @param properties
	 * @return
	 */
	private void registerCassandraSession(String resourceName,
			Properties properties) {
		if (context.containsKey(resourceName))
			throw new IllegalArgumentException("resource " + resourceName
					+ " already set in the context. Override is not allowed");

		Builder builderCluster = null;
		
		// define the exact datastax cluster builder (Community or Enterprise)
		// according to the value set for
		// the property datastax.driverClassName
		try {
			builderCluster = DatastaxDriverUtils
					.defineClusterBuilder(properties.getProperty(DATASTAX_DRIVERCLASSNAME_PROPERTY));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		configureBuilderCluster(properties, builderCluster);

		Cluster cluster = builderCluster.build();

		Session session = cluster.connect(properties
				.getProperty(CASSANDRA_KEYSPACE));

		context.put(resourceName, session);

	}

	// Configuring the query options
	private QueryOptions buildQueryOptions() {

		QueryOptions queryOptions = new QueryOptions();

		queryOptions.setFetchSize(QueryOptions.DEFAULT_FETCH_SIZE);
		queryOptions.setConsistencyLevel(ConsistencyLevel.LOCAL_ONE);
		queryOptions.setSerialConsistencyLevel(ConsistencyLevel.SERIAL);
		queryOptions.setReprepareOnUp(Boolean.TRUE);
		queryOptions.setPrepareOnAllHosts(Boolean.TRUE);

		return queryOptions;

	}

	// Configure policy that decides how often the reconnection to a dead node
	// is attempted
	private ReconnectionPolicy buildReconnectionPolicy() {
		
		Long baseDelayMs = Long.valueOf(System.getProperty(BASE_DELAY_MS, "10"));
		Long maxDelayMs  = Long.valueOf(System.getProperty(MAX_DELAY_MS, "10"));

		return new ExponentialReconnectionPolicy(baseDelayMs, maxDelayMs);

	}

	// Configuring the connection pool
	private PoolingOptions buildPoolingOptions() {

		int localCoreConnections = Integer.valueOf(System.getProperty(LOCAL_CORE_CONNECTIONS, "4"));
		int localMaxConnections = Integer.valueOf(System.getProperty(LOCAL_MAX_CONNECTIONS, "10"));
		int remoteCoreConnections = Integer.valueOf(System.getProperty(REMOTE_CORE_CONNECTIONS, "4"));
		int remoteMaxConnections = Integer.valueOf(System.getProperty(REMOTE_MAX_CONNECTIONS, "10"));

		PoolingOptions poolingOptions = new PoolingOptions();
		poolingOptions.setConnectionsPerHost(HostDistance.LOCAL,
				localCoreConnections, localMaxConnections);
		poolingOptions.setConnectionsPerHost(HostDistance.REMOTE,
				remoteCoreConnections, remoteMaxConnections);

		return poolingOptions;
	}

	// Configure SocketOptions to control various low-level parameters related
	// to TCP connections between the driver and Cassandra
	private SocketOptions buildSocketOptions() {

		SocketOptions socketOptions = new SocketOptions();

		socketOptions.setKeepAlive(Boolean.FALSE);
		socketOptions
				.setConnectTimeoutMillis(SocketOptions.DEFAULT_CONNECT_TIMEOUT_MILLIS);
		socketOptions
				.setReadTimeoutMillis(SocketOptions.DEFAULT_READ_TIMEOUT_MILLIS);
		socketOptions.setReuseAddress(Boolean.FALSE);
		socketOptions.setTcpNoDelay(Boolean.TRUE);

		return socketOptions;

	}

	// Set properties for Cassandra cluster configuration
	private void configureBuilderCluster(Properties properties,
			Builder builderCluster) {

		String cassandraPassword = properties.getProperty(CASSANDRA_PASSWORD);

		Boolean isCassandraSsl = Boolean.valueOf(properties
				.getProperty(CASSANDRA_SSL_ENABLED));

		if (cassandraPassword == null) {
			throw new IllegalArgumentException("Property '"
					+ CASSANDRA_PASSWORD + "' undefined!");
		}

		// This system property is needed to resolve datastax.driver.core.Native
		// exception since could not load JNR C Library, native system calls
		// through this library will not be available
		System.setProperty("com.datastax.driver.USE_NATIVE_CLOCK",
				Boolean.FALSE.toString());

		builderCluster
				.addContactPoint(properties.getProperty(CASSANDRA_HOST))
				.withPort(
						Integer.valueOf(properties.getProperty(CASSANDRA_PORT)))
				.withCredentials(properties.getProperty(CASSANDRA_USERNAME),
						cassandraPassword)
				.withQueryOptions(buildQueryOptions())
				.withPoolingOptions(buildPoolingOptions())
				.withReconnectionPolicy(buildReconnectionPolicy())
				.withSocketOptions(buildSocketOptions());

		// Set properties for SSL connection
		if (isCassandraSsl) {

			System.setProperty(
					"javax.net.ssl.trustStore",
					extractMandatorySslPropertyValue(CASSANDRA_SSL_TRUST_STORE,
							properties));
			System.setProperty(
					"javax.net.ssl.trustStorePassword",
					extractMandatorySslPropertyValue(
							CASSANDRA_SSL_TRUST_STORE_PASSWORD, properties));

			builderCluster.withSSL();

		}

	}

	protected String extractMandatorySslPropertyValue(String propertyKey,
			Properties dictionary) {
		if (dictionary == null) {
			throw new IllegalArgumentException("Property dictionary null!");
		}
		if (StringUtils.isBlank(propertyKey)) {
			throw new IllegalArgumentException("Property key is blank!");
		}
		String property = dictionary.getProperty(propertyKey);
		if (StringUtils.isBlank(property)) {
			throw new IllegalArgumentException("Unable to find property '"
					+ propertyKey
					+ "' to enable SSL connection. It is mandatory! ");
		}
		return property;

	}

}
