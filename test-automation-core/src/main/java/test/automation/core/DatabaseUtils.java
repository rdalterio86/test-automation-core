package test.automation.core;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * This class provides utilities method to get pre-configured instances for
 * storage systems (database). Supported databases are: 1. Relational Database
 * (Oracle) 2. NoSql Database (Cassandra)
 */
public class DatabaseUtils {

	private static DatabaseUtils instance;

	private DatabaseUtils() {

	}

	/**
	 * singleton factory method
	 * 
	 * @return single instance of DatabaseUtils
	 * @deprecated As of test-automation-core 1.7, replaced by
	 *             <code>DatabaseUtils.jdbc()</code> or
	 *             <code>DatabaseUtils.cassandra()</code>.
	 */
	@Deprecated
	public static DatabaseUtils db() {
		if (instance != null) {
			return instance;
		}

		synchronized (DatabaseUtils.class) {
			if (instance != null) {
				return instance;
			}

			instance = new DatabaseUtils();
			return instance;

		}
	}

	/**
	 * 
	 * @return single instance of JdbcDatabaseUtils
	 */
	public static JdbcDatabaseUtils jdbc() {

		return JdbcDatabaseUtils.getInstance();
	}

	/**
	 * 
	 * @return single instance of CassandraDatabaseUtils
	 */
	public static CassandraDatabaseUtils cassandra() {

		return CassandraDatabaseUtils.getInstance();
	}

	/**
	 * @deprecated As of test-automation-core 1.7, replaced by
	 *             <code>DatabaseUtils.jdbc().template()</code>
	 */
	@Deprecated
	public JdbcTemplate template() {

		return JdbcDatabaseUtils.getInstance().template();

	}

	/**
	 * @deprecated As of test-automation-core 1.7, replaced by
	 *             <code>DatabaseUtils.jdbc().template(String resourceName)</code>
	 */
	@Deprecated
	public JdbcTemplate template(String resourceName) {

		return JdbcDatabaseUtils.getInstance().template(resourceName);

	}

}
