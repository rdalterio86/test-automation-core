package test.automation.core;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import test.automation.core.benchmark.BenchmarkableAnnotationObject;
import test.automation.core.benchmark.BenchmarkableContext;
import test.automation.core.benchmark.UiBenchmarkableContext;
import test.automation.core.har.HarFileUtils;
import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

public class UIUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(UIUtils.class);

	public static final String DRIVER_TARGET_URL = "driver.target.url";

	/**
	 * Driver type. [firefox|chrome|iexplorer|electron|mobile]
	 */
	private static final String DRIVER_TYPE = "driver.type";

	public static final String DRIVER_TYPE_CHROME = "chrome";
	public static final String DRIVER_TYPE_FIREFOX = "firefox";
	public static final String DRIVER_TYPE_ELECTRON = "electron";
	public static final String DRIVER_TYPE_IEXPLORER = "iexplorer";
	public static final String DRIVER_TYPE_MOBILE = "mobile";
	public static final String DRIVER_TYPE_REMOTE = "remote";
	public static final String CHROME_DRIVER = "chrome.driver";
	public static final String IE_DRIVER = "ie.driver";
	public static final String ELECTRON_BINARY = "electron.binary";
	public static final String ELECTRON_APP_ARGS = "electron.app.args";
	public static final String REPORT_IMAGES_DIR = "report.images.dir";
	public static final String DEFAULT_REPORT_IMAGES_DIR = "embedded-images";
	public static final String SCENARIO_ID = "scenario.calculated.id";

	// android properties specific
	public static final String ANDROID_PLATFORM_VERSION = "android.platform.version";
	public static final String ANDROID_DEVICE_NAME = "android.device.name";
	public static final String ANDROID_APP_PACKAGE = "android.app.package";
	public static final String ANDROID_APP_ACTIVITY = "android.app.activity";
	public static final String ANDROID_SERVER_URL = "android.server.url";
	public static final String ANDROID_NEW_COMMAND_TIMEOUT = "android.new.command.timeout";

	// properties for grid configuration
	public static final String GRID_SERVER_URL = "grid.server.url";
	public static final String GRID_BROWSER_TYPE = "grid.browser.type";
	public static final String GRID_BROWSER_VERSION = "grid.browser.version";
	public static final String GRID_PLATFORM_TYPE = "grid.platform.type";

	// Fix Sonar critical issue
	private static final long DEFAULT_EXPLICT_WAIT_TIMEOUT = 10L;
	private static final long DEFAULT_IMPLICIT_WAIT_TIMEOUT = 10L;

	private static UIUtils instance;

	private Map<String, Properties> propertiesBundles = null;

	protected WebDriver currentDriver = null;

	private Properties p = null;

	private UIUtils() {
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);

		propertiesBundles = multiPropertiesLoader.loadAndCheck((d, name) -> name.endsWith("-ui.properties"),
				DRIVER_TYPE);
	}

	public synchronized static UIUtils ui() {
		if (instance == null) {
			instance = new UIUtils();
		}
		return instance;
	}

	/**
	 * Build a SeleniumWebDriver configured using
	 * <configurationType>-ui.properties If the properties 'driver.target.url'
	 * is set, the driver will point to that URL.
	 * 
	 * @param configurationType
	 * @return the Selenium WebDriver configured. The implicit wait is 10
	 *         seconds.
	 */
	public WebDriver driver(String configurationType) {
		this.currentDriver = buildDriverWith(configurationType);
		return currentDriver;
	}

	/**
	 * Build a SeleniumWebDriver configured using default-ui.properties
	 *
	 * @return the Selenium WebDriver configured
	 */
	public WebDriver driver() {
		return driver("default");
	}

	private void createScreenshotDir(Properties p) {
		String workingDir = System.getProperty("user.dir");
		StringBuilder sb = new StringBuilder();
		String reportImageDir = p.getProperty(REPORT_IMAGES_DIR, DEFAULT_REPORT_IMAGES_DIR);
		sb.append(workingDir).append(File.separatorChar).append("target").append(File.separatorChar)
				.append(reportImageDir);
		try {
			FileUtils.forceMkdir(new File(sb.toString()));
			System.setProperty(REPORT_IMAGES_DIR, sb.toString());
		} catch (IOException e) {
			throw new RuntimeException("Unable to create report image dir " + sb.toString());
		}
	}

	/**
	 * Quits this driver, closing every associated window.
	 */
	public void closeDriver(WebDriver driver) {

		BenchmarkableContext context = AutomationThreadLocal.get();
		if (driver != null) {
			if (context != null) {
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("dynaTrace.endVisit();");

			}

			driver.quit();
		}

	}

	/**
	 * Wait for a function evaluation using this time in second Method usage:
	 * 
	 * <pre>
	 * WebElement element = UIUtils.ui().waitForElement(driver,
	 * 		ExpectedConditions.visibilityOfElementLocated(By.id("someid")), 10);
	 * </pre>
	 * 
	 * @param driver
	 * @param expectedCondition
	 *            ExpectedCondition for that element
	 * @return T The function's expected return type.
	 */
	public <T> T waitForCondition(WebDriver driver, ExpectedCondition<T> expectedCondition) {
		return waitForCondition(driver, expectedCondition, DEFAULT_EXPLICT_WAIT_TIMEOUT);
	}

	/**
	 * Wait for an element using this time in second Method usage:
	 * 
	 * <pre>
	 * WebElement element = UIUtils.ui().waitForElement(driver,
	 * 		ExpectedConditions.visibilityOfElementLocated(By.id("someid")), 10);
	 * </pre>
	 * 
	 * @param driver
	 * @param expectedCondition
	 *            ExpectedCondition for that element
	 * @param timeout
	 *            timeout in second
	 * @return T The function's expected return type.
	 */
	public <T> T waitForCondition(WebDriver driver, ExpectedCondition<T> expectedCondition, long timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		T value = wait.until(expectedCondition);
		return wait.until(expectedCondition);
	}

	private WebDriver buildDriverWith(String configurationType) {
		p = propertiesBundles.get(configurationType);

		if (p == null) {
			throw new IllegalArgumentException("Unable to find configuration for type: " + configurationType);
		}

		createScreenshotDir(p);
		String driverType = p.getProperty(DRIVER_TYPE);

		// get har.file.enabled property for the built driver

		WebDriver driverBuilt = null;
		if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_FIREFOX)) {
			driverBuilt = buildFirefoxDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_CHROME)) {
			driverBuilt = buildChromeDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_IEXPLORER)) {
			driverBuilt = buildIEDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_ELECTRON)) {
			driverBuilt = buildElectronDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_MOBILE)) {
			driverBuilt = buildAndroidDriver(p);
		} else if (StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_REMOTE)) {
			try {
				driverBuilt = buildRemoteDriver(p);
			} catch (MalformedURLException e) {
				throw new TestConfigurationException(
						"A malformed URL has occurred configuring test property for grid server url!", e);
			}
		} else {
			throw new UnsupportedOperationException("Unsupported driver type: " + driverType);
		}

		// set the browser url to target URL for driver type not equals to
		// mobile
		if (!StringUtils.equalsIgnoreCase(driverType, DRIVER_TYPE_MOBILE)) {
			String targetURL = p.getProperty(DRIVER_TARGET_URL);
			if (StringUtils.isNotBlank(targetURL)) {
				driverBuilt.get(targetURL);
			}
		}

		performUiMonitoring(driverBuilt);
		driverBuilt.manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);

		return driverBuilt;

	}

	private void performUiMonitoring(WebDriver driverBuilt) {
		UiBenchmarkableContext context = (UiBenchmarkableContext) AutomationThreadLocal.get();
		if (context != null) {
			List<BenchmarkableAnnotationObject> benchmarkableObjectList = context.getBenchmarkableObjectList();
			JavascriptExecutor js = (JavascriptExecutor) driverBuilt;

			BenchmarkableAnnotationObject benchmarkableObject = benchmarkableObjectList.get(0);
			String testRunId = context.getTestRunId();

			js.executeScript("sessionStorage.DT_TESTRUNID = \"" + testRunId + "\";");
			js.executeScript("sessionStorage.DT_TESTNAME = \"" + benchmarkableObject.getName() + "\";");

			context.setDriver(driverBuilt);
			AutomationThreadLocal.unset();
			AutomationThreadLocal.set(context);

		}
	}

	private WebDriver buildAndroidDriver(Properties p) {
		// build an AndroidDriver using a capabilities
		DesiredCapabilities caps = new DesiredCapabilities();

		String platformVersion = extractMandatoryPropertyValue(ANDROID_PLATFORM_VERSION, p);
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);

		String deviceName = extractMandatoryPropertyValue(ANDROID_DEVICE_NAME, p);
		caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);

		// Move directly into Webview context allowing you to avoid context
		// switch for Hybrid mobile app testing. Default false.
		caps.setCapability(MobileCapabilityType.AUTO_WEBVIEW, Boolean.TRUE);

		String appPackage = extractMandatoryPropertyValue(ANDROID_APP_PACKAGE, p);
		caps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, appPackage);

		String appActivity = extractMandatoryPropertyValue(ANDROID_APP_ACTIVITY, p);
		caps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, appActivity);

		// this property is not mandatory
		String chromeDriverMobile = p.getProperty(CHROME_DRIVER);
		if (StringUtils.isNotBlank(chromeDriverMobile)) {
			caps.setCapability(AndroidMobileCapabilityType.CHROMEDRIVER_EXECUTABLE, chromeDriverMobile);
		}

		// Not mandatory property to define how long Appium will wait for a new
		// command before ending the session
		String newCommandTimeOut = p.getProperty(ANDROID_NEW_COMMAND_TIMEOUT);
		if (StringUtils.isNotBlank(newCommandTimeOut)) {
			caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, newCommandTimeOut);
		}

		String serverUrlAsString = extractMandatoryPropertyValue(ANDROID_SERVER_URL, p);

		try {
			AndroidDriver tempDriver = new AndroidDriver<>(new URL(serverUrlAsString), caps);

			return tempDriver;
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException("Error creating androidDriver. ", e);
		}
	}

	private WebDriver buildFirefoxDriver(Properties p) {

		if (HarFileUtils.isHarFile()) {
			// configure Selenium proxy object as a desired capability
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));

			return new FirefoxDriver(capabilities);

		} else {
			return new FirefoxDriver();
		}
	}

	private WebDriver buildChromeDriver(Properties p) {
		String chromeDriver = extractMandatoryPropertyValue(CHROME_DRIVER, p);
		System.setProperty("webdriver.chrome.driver", chromeDriver);

		if (HarFileUtils.isHarFile()) {
			// configure Selenium proxy object as a desired capability
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));

			return new ChromeDriver(capabilities);

		} else {
			return new ChromeDriver();
		}
	}

	private WebDriver buildIEDriver(Properties p) {
		String ieDriver = extractMandatoryPropertyValue(IE_DRIVER, p);
		System.setProperty("webdriver.ie.driver", ieDriver);

		DesiredCapabilities capabilities = new DesiredCapabilities();

		if (HarFileUtils.isHarFile()) {
			// configure Selenium proxy object as a desired capability
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));

			return new InternetExplorerDriver(capabilities);
		} else {
			capabilities.setCapability(InternetExplorerDriver.IE_USE_PRE_PROCESS_PROXY, true);

			return new InternetExplorerDriver(capabilities);

		}
	}

	private WebDriver buildElectronDriver(Properties p) {
		String chromeDriver = extractMandatoryPropertyValue(CHROME_DRIVER, p);
		String electronBinary = extractMandatoryPropertyValue(ELECTRON_BINARY, p);

		// mandatory for selenium
		System.setProperty("webdriver.chrome.driver", chromeDriver);
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		ChromeOptions options = new ChromeOptions();
		options.setBinary(electronBinary);
		String appArgs = p.getProperty(ELECTRON_APP_ARGS);

		if (StringUtils.isNotBlank(appArgs)) {

			addArgsToElectronApp(options, appArgs);

		}

		DesiredCapabilities capabilities = new DesiredCapabilities();

		if (HarFileUtils.isHarFile()) {
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setBrowserName(DRIVER_TYPE_ELECTRON);
			// configure Selenium proxy object as a desired capability
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
			return new ChromeDriver(capabilities);

		} else {
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setBrowserName(DRIVER_TYPE_ELECTRON);
			return new ChromeDriver(capabilities);
		}

	}

	/**
	 * Add arguments to the electron application under test in the format
	 * Key=Value.
	 * 
	 */
	private void addArgsToElectronApp(ChromeOptions options, String appArgs) {
		// The value of property electron.app.args must be enclosed in
		// single curly braces.
		// You need to set a comma-separated list of key and value pairs.
		int lastIndex = appArgs.length() - 1;
		char first = appArgs.charAt(0);
		char last = appArgs.charAt(lastIndex);

		if ((first == '{') && (last == '}')) {
			String arguments = appArgs.substring(1, lastIndex);
			String[] args = arguments.split(",");
			List<String> argsList = Arrays.asList(args);

			for (String arg : argsList) {

				// To start Electron application using custom command line
				// arguments
				// you need to specify arguments in the format
				// <Key>=<value>.
				Pattern keyValuePattern = Pattern.compile("(.+?)=(.+?)");
				Matcher matcherKeyValuePattern = keyValuePattern.matcher(arg);

				if (!matcherKeyValuePattern.find()) {
					throw new IllegalArgumentException("The argument " + arg
							+ " of list specified in electron.app.args property must be in the format <Key>=<value>!");
				}

				// The property electron.app.args cannot take the key of
				// each
				// custom argument in camelCase format but with hyphens
				// syntax
				String key = arg.split("=")[0];
				Pattern camelCasePattern = Pattern.compile("[a-z]+[A-Z0-9][a-z0-9]+[A-Za-z0-9]*");
				Matcher matcherCamelCasePattern = camelCasePattern.matcher(key);

				if (matcherCamelCasePattern.find()) {
					throw new IllegalArgumentException("The property electron.app.args cannot take the key of argument "
							+ arg
							+ " in camelCase format but with hyphens syntax! E.g., the key electronPort needs to be changed in electron-port!");
				}

			}
			options.addArguments(argsList);

		} else {

			throw new IllegalArgumentException(
					"The value of property electron.app.args must be enclosed in single curly braces! It must be a comma-separated list of key and value pairs that represents the custom command line arguments of electron application! E.g., {webpack-port=3000,electron-port=8000}");

		}
	}

	/**
	 * Create Selenium RemoteWebDriver to allow UI test execution inside a grid.
	 * 
	 */
	private WebDriver buildRemoteDriver(Properties p) throws MalformedURLException {
		String gridServerUrl = extractMandatoryPropertyValue(GRID_SERVER_URL, p);
		String browserType = extractMandatoryPropertyValue(GRID_BROWSER_TYPE, p);
		String electronAppBinary = p.getProperty(ELECTRON_BINARY);
		String platformType = p.getProperty(GRID_PLATFORM_TYPE);
		String browserVersion = p.getProperty(GRID_BROWSER_VERSION);
		String browserTargetURL = p.getProperty(DRIVER_TARGET_URL);
		String electronAppArgs = p.getProperty(ELECTRON_APP_ARGS);

		DesiredCapabilities capabilities = null;

		if (browserType.equalsIgnoreCase(DRIVER_TYPE_CHROME)) {
			capabilities = setChromeCapabilities(p, electronAppBinary, browserVersion, browserTargetURL,
					electronAppArgs);
		} else if (browserType.equalsIgnoreCase(DRIVER_TYPE_FIREFOX)) {
			capabilities = DesiredCapabilities.firefox();
			if (HarFileUtils.isHarFile()) {
				capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
			}
		} else if (browserType.equalsIgnoreCase(DRIVER_TYPE_IEXPLORER)) {
			capabilities = DesiredCapabilities.internetExplorer();
			if (HarFileUtils.isHarFile()) {
				capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
			} else {
				capabilities.setCapability(InternetExplorerDriver.IE_USE_PRE_PROCESS_PROXY, true);
			}
		} else {
			throw new IllegalArgumentException("Unsupported browser type: " + browserType
					+ ". Supported browser types are: chrome, firefox, iexplorer!");
		}

		if (StringUtils.isNotBlank(platformType)) {
			capabilities.setCapability("platform", platformType);
		}

		if (StringUtils.isNotBlank(browserVersion)) {
			capabilities.setVersion(browserVersion);
		}

		return new RemoteWebDriver(new URL(gridServerUrl), capabilities);
	}

	/**
	 * Set capabilities for browser type "Chrome" inside the grid (It includes
	 * also electron applications).
	 * 
	 */
	private DesiredCapabilities setChromeCapabilities(Properties p, String electronAppBinary, String browserVersion,
			String browserTargetURL, String electronAppArgs) {
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		if ((StringUtils.isNotBlank(electronAppBinary)) && (StringUtils.isNotBlank(browserVersion))) {
			throw new IllegalArgumentException("Cannot specify browser version for electron app test inside grid!");
		}
		if ((StringUtils.isNotBlank(electronAppBinary)) && (StringUtils.isNotBlank(browserTargetURL))) {
			throw new IllegalArgumentException("Cannot specify browser target URL for electron app test inside grid!");
		}
		if (StringUtils.isNotBlank(electronAppBinary)) {
			ChromeOptions options = new ChromeOptions();
			options.setBinary(electronAppBinary);
			if (StringUtils.isNotBlank(electronAppArgs)) {

				addArgsToElectronApp(options, electronAppArgs);

			}
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}
		if (HarFileUtils.isHarFile()) {
			capabilities.setCapability(CapabilityType.PROXY, HarFileUtils.buildSeleniumProxy(p));
		}
		return capabilities;
	}

	/**
	 * Check if the element find by this locator is contained in the DOM
	 * 
	 * @param locator
	 *            The locator used to find the element
	 * @return true if the element is not present, false otherwise
	 */
	public ExpectedCondition<Boolean> elementNotPresent(By locator) {
		return new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				List<WebElement> findElements = driver.findElements(locator);
				driver.manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);
				return findElements.size() == 0;
			}
		};
	}

	/**
	 * Take a screenshot. The image will be scaled with 1/2 aspect ratio
	 * calculated on current screen dimension. The image will be saved in the
	 * report images directory
	 * 
	 * @param driver
	 * @return the file name created
	 */
	public String takeScreenshot(final WebDriver driver) {
		String fileName = "screenshot_" + System.getProperty(UIUtils.SCENARIO_ID) + ".png";
		String reportDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
		String fileNameAbsolutePath = reportDir + File.separatorChar + fileName;
		try {
			File image = null;
			if (!(driver instanceof AndroidDriver)) {
				// BufferedImage scaled = UISupport.captureScreenshoot();
				// ImageIO.write( scaled, "png", new File(fileNameAbsolutePath)
				// );
				image = UISupport.captureScreenshoot(driver);
			} else {
				image = UISupport.captureScreenshoot((AndroidDriver) driver);
			}
			FileUtils.copyFile(image, new File(fileNameAbsolutePath));
		} catch (Exception e) {
			throw new RuntimeException("Error taking screenshot.", e);
		}
		return fileName;
	}

	/**
	 * Take a screenshot. The image will be scaled with 1/2 aspect ratio
	 * calculated on current screen dimension. The image will be saved in the
	 * report images directory
	 * 
	 * @param driver
	 * @deprecated will be removed in next version
	 */
	public void takeScreenshot() {
		try {
			String reportDir = System.getProperty(UIUtils.REPORT_IMAGES_DIR);
			String fileName = "screenshot_" + System.getProperty(UIUtils.SCENARIO_ID) + ".png";
			String fileNameAbsolutePath = reportDir + File.separatorChar + fileName;
			BufferedImage scaled = UISupport.captureScreenshoot();
			ImageIO.write(scaled, "png", new File(fileNameAbsolutePath));
		} catch (Exception e) {
			throw new RuntimeException("Error taking screenshot.", e);
		}
	}

	protected String extractMandatoryPropertyValue(String propertyKey, Properties dictionary) {
		if (dictionary == null) {
			throw new IllegalArgumentException("Property dictionary null!");
		}
		if (StringUtils.isBlank(propertyKey)) {
			throw new IllegalArgumentException("Property key is blank!");
		}
		String property = dictionary.getProperty(propertyKey);
		if (StringUtils.isBlank(property)) {
			throw new IllegalArgumentException("Unable to find property '" + propertyKey + "'. It is mandatory! ");
		}
		return property;

	}

	/**
	 * for internal use only
	 * 
	 * @return
	 */
	public WebDriver getCurrentDriver() {
		return this.currentDriver;
	}

	/**
	 * for internal use only
	 * 
	 * @return
	 */
	public Properties getCurrentProperties() {
		return this.p;
	}

}
