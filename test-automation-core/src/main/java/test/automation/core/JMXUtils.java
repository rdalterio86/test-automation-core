package test.automation.core;

import static javax.management.remote.JMXConnector.CREDENTIALS;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import org.springframework.jmx.support.MBeanServerConnectionFactoryBean;

import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;

public class JMXUtils {
	private static JMXUtils instance;
	private Map<String, MBeanServerConnection> context;
	private MBeanServerConnection mbeanServerConnection;
	private final String DEFAULT_JCONSOLE_PROPERTIES_BUNDLE_KEY = "default";
	private static final String SERVICE_URL = "jmx.service.url";
	private final static String AUTH_BASIC_USERNAME = "auth.basic.username";
	private final static String AUTH_BASIC_PASSWORD = "auth.basic.password";

	/**
	 * Loading properties file from classpath.
	 */
	private JMXUtils() {
		context = new HashMap<String, MBeanServerConnection>();
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);
		Map<String, Properties> propertiesBundles = multiPropertiesLoader
				.loadAndCheck((d, name) -> name.endsWith("-jmx.properties"), SERVICE_URL);
		if (!propertiesBundles.containsKey(DEFAULT_JCONSOLE_PROPERTIES_BUNDLE_KEY))
			throw new IllegalArgumentException("No default-jmx.properties found in the "
					+ ((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath());
		MBeanServerConnection mbeanServerConnection = null;
		for (Entry<String, Properties> propertyBundle : propertiesBundles.entrySet()) {
			mbeanServerConnection = initTemplate(propertyBundle.getKey(), propertyBundle.getValue());
			context.put(propertyBundle.getKey(), mbeanServerConnection);
		}
	}

	public JMXUtils template() {
		mbeanServerConnection = context.get("default");
		return this;
	}

	public JMXUtils template(String tamplateKey) {
		mbeanServerConnection = context.get(tamplateKey);
		if (mbeanServerConnection == null)
			throw new IllegalArgumentException(
					"No MBean server connection template available for jmx service url [" + tamplateKey + "]");
		return this;
	}

	/**
	 * This method invokes an operation on an MBean.
	 * 
	 * @param mbeanName
	 *            The name of the MBean on which the method is to be invoked.
	 * @param operationName
	 *            The name of the operation to be invoked.
	 * @param params
	 *            An array containing the parameters to be set when the
	 *            operation is invoked. (It's important the parameters order)
	 * @return The object returned by the operation, which represents the result
	 *         of invoking the operation on the MBean specified.
	 * @throws InstanceNotFoundException
	 */
	public Object invokeOperation(String mbeanName, String operationName, List<Object> params)
			throws InstanceNotFoundException {
		ObjectName mbean;
		Object result = null;
		try {
			mbean = new ObjectName(mbeanName);
		} catch (MalformedObjectNameException e) {
			String errorMessage = "Error during ObjectName creation: " + mbeanName;
			throw new RuntimeException(errorMessage, e);
		}
		try {
			if (!mbeanServerConnection.isRegistered(mbean)) {
				throw new InstanceNotFoundException(
						"The MBean specific is not registered in the MBean server. MBean [" + mbeanName + "]");
			}
		} catch (IOException e) {
			String errorMessage = "Error during checks whether an MBean is already registered [" + mbeanName + "]";
			throw new RuntimeException(errorMessage, e);
		}
		List<String> signatureList = new ArrayList<String>();
		for (Object parameter : params) {
			signatureList.add(parameter.getClass().getTypeName());
		}
		try {
			result = mbeanServerConnection.invoke(mbean, operationName, params.toArray(),
					signatureList.toArray(new String[params.size()]));
		} catch (InstanceNotFoundException | MBeanException | ReflectionException | IOException e) {
			String errorMessage = "Error during invocation of operation [" + operationName + "] on MBean [" + mbeanName
					+ "]";
			throw new RuntimeException(errorMessage, e);
		}
		return result;
	}

	private MBeanServerConnection initTemplate(String templateKey, Properties properties) {
		HashMap<String, String[]> env = new HashMap<String, String[]>();
		String[] credentials = new String[] { properties.getProperty(AUTH_BASIC_USERNAME),
				properties.getProperty(AUTH_BASIC_PASSWORD) };
		env.put(CREDENTIALS, credentials);
		MBeanServerConnectionFactoryBean mbeanServerConnectionFactoryBean = new MBeanServerConnectionFactoryBean();
		mbeanServerConnectionFactoryBean.setEnvironmentMap(env);
		try {
			mbeanServerConnectionFactoryBean.setServiceUrl(properties.getProperty(SERVICE_URL));
		} catch (MalformedURLException e) {
			String errorMessage = "Error setting the service URL of the remote MBeanServer [" + SERVICE_URL + "]";
			throw new RuntimeException(errorMessage, e);
		}
		try {
			mbeanServerConnectionFactoryBean.afterPropertiesSet();
		} catch (Exception e) {
			String errorMessage = "Error during jmx connection factory retrieval from server ["
					+ properties.getProperty(SERVICE_URL) + "]";
			throw new RuntimeException(errorMessage, e);
		}
		MBeanServerConnection mbeanServerConnection = mbeanServerConnectionFactoryBean.getObject();
		return mbeanServerConnection;
	}

	/**
	 * Singleton factory method
	 * 
	 * @return Single instance of JconsoleUtils
	 */
	public static JMXUtils jmx() {
		if (instance != null) {
			return instance;
		}
		synchronized (JMXUtils.class) {
			if (instance != null) {
				return instance;
			}
			instance = new JMXUtils();
			return instance;
		}
	}
}
