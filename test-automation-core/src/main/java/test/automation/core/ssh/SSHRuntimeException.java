package test.automation.core.ssh;

public class SSHRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public SSHRuntimeException() {
		super();
	}

	public SSHRuntimeException(String message) {
		super(message);
	}

	public SSHRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public SSHRuntimeException(Throwable cause) {
		super(cause);
	}

	protected SSHRuntimeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
