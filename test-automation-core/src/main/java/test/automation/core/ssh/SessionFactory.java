package test.automation.core.ssh;

import java.io.File;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class SessionFactory {
	private final static String CHAR_DOT = ".";
	private final static String SSH_HOST = "ssh.host";
	private final static String SSH_PORT = "ssh.port";
	private final static String SSH_USERNAME = "ssh.username";
	private final static String SSH_PASSWORD = "ssh.password";
	private final static String SSH_PRIVATE_KEY_PATH = "ssh.private.key.path";
	private final static String SSH_PASSPHRASE = "ssh.passphrase";
	private final static String SSH_CONFIG = "ssh.config";
	private final static String STRICT_HOSTKEY_CHECKING_KEY = "StrictHostKeyChecking";
	private final static String PREFERRED_AUTHENTICATIONS_KEY = "PreferredAuthentications";
	private final static String CONFIG_STRICT_HOSTKEY_CHECKING_KEY = SSH_CONFIG + CHAR_DOT
			+ STRICT_HOSTKEY_CHECKING_KEY;
	private final static String CONFIG_PREFERRED_AUTHENTICATIONS_KEY = SSH_CONFIG + CHAR_DOT
			+ PREFERRED_AUTHENTICATIONS_KEY;
	private final static String STRICT_HOSTKEY_CHECKING_VALUE = "no";
	private final static String PREFERRED_AUTHENTICATIONS_VALUE = "publickey,keyboard-interactive,password";
	private static final Logger LOGGER = LoggerFactory.getLogger(SessionFactory.class);

	private SessionFactory() {
	}

	public static Session createSession(Properties properties) {
		Session session = null;
		try {
			checkMandatory(properties);
			JSch jsch = new JSch();
			/* Authentication by private key and passphrase */
			if (properties.containsKey(SSH_PRIVATE_KEY_PATH) || properties.containsKey(SSH_PASSPHRASE)) {
				if (StringUtils.isNotBlank(properties.getProperty(SSH_PRIVATE_KEY_PATH))
						&& StringUtils.isNotBlank(properties.getProperty(SSH_PASSPHRASE))) {
					File fileKeys = new File(properties.getProperty(SSH_PRIVATE_KEY_PATH));
					if (!fileKeys.exists()) {
						throw new SSHRuntimeException(
								"Key files not found in path " + properties.getProperty(SSH_PRIVATE_KEY_PATH));
					}
					jsch.addIdentity(fileKeys.getAbsolutePath(), properties.getProperty(SSH_PASSPHRASE));
				} else {
					throw new SSHRuntimeException("Private key or Passphrase property is not found!");
				}
			}
			if (StringUtils.isBlank(properties.getProperty(SSH_PORT))) {
				/* getSession sets automatically the default port (22) */
				session = jsch.getSession(properties.getProperty(SSH_USERNAME), properties.getProperty(SSH_HOST));
				LOGGER.warn("ssh.port property key was not found, default port (22) has been set.");
			} else {
				session = jsch.getSession(properties.getProperty(SSH_USERNAME), properties.getProperty(SSH_HOST),
						Integer.parseInt(properties.getProperty(SSH_PORT)));
			}
			session = initSession(properties, session);
		} catch (JSchException jschX) {
			throw new SSHRuntimeException("Session connection exception for Host[" + properties.getProperty(SSH_HOST)
					+ "], Port[" + properties.getProperty(SSH_PORT) + "]", jschX);
		}
		return session;
	}

	/**
	 * 
	 * Check if the mandatory properties (host and username) are configurated in
	 * properties file
	 * 
	 * @param properties
	 */
	private static void checkMandatory(Properties properties) {
		if (StringUtils.isBlank(properties.getProperty(SSH_HOST))) {
			throw new SSHRuntimeException(
					"ssh.host property key not specified, but it must be set in order to connect");
		}
		if (StringUtils.isBlank(properties.getProperty(SSH_USERNAME))) {
			throw new SSHRuntimeException(
					"ssh.username property key not specified, but it must be set in order to connect");
		}
	}

	/**
	 * 
	 * Set default values: StrictHostKeyChecking = no PreferredAuthentications =
	 * publickey,keyboard-interactive,password *
	 * 
	 * @param properties
	 * @param session
	 */
	private static Session setDefault(Properties properties, Session session) {
		if (StringUtils.isBlank(properties.getProperty(CONFIG_STRICT_HOSTKEY_CHECKING_KEY))) {
			session.setConfig(STRICT_HOSTKEY_CHECKING_KEY, STRICT_HOSTKEY_CHECKING_VALUE);
		}
		if (StringUtils.isBlank(properties.getProperty(CONFIG_PREFERRED_AUTHENTICATIONS_KEY))) {
			session.setConfig(PREFERRED_AUTHENTICATIONS_KEY, PREFERRED_AUTHENTICATIONS_VALUE);
		}
		return session;
	}

	/**
	 * Session initialization with properties
	 * 
	 * @param properties
	 */
	protected static Session initSession(Properties properties, Session session) throws JSchException {
		session = setDefault(properties, session);
		if (StringUtils.isNotBlank(properties.getProperty(SSH_PASSWORD))) {
			session.setPassword(properties.getProperty(SSH_PASSWORD));
		}
		/* read and set all properties that starts with "ssh.config" */
		Set<String> keys = properties.stringPropertyNames();
		for (String key : keys) {
			if (key.startsWith(SSH_CONFIG)) {
				session.setConfig(key.substring(key.lastIndexOf(CHAR_DOT) + 1), properties.getProperty(key));
			}
		}
		session.connect();
		return session;
	}
}
