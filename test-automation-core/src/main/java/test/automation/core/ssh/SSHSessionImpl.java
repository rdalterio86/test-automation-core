package test.automation.core.ssh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import test.automation.core.SSHUtils;

public class SSHSessionImpl implements SSHSession {
	private static final String EXEC = "exec";
	private final static String DOUBLE_AMP = "&&";
	private static final int DEFAULT_TIMEOUT = 60000;
	private static final int SLEEP_TIME = 100;
	//Fix Sonar critical issue
    private static final String PORT_STRING = "] port [";
	private Logger logger = LoggerFactory.getLogger(SSHUtils.class);
	private Properties properties = null;

	/**
	 * 
	 * Constructor class
	 * 
	 */
	public SSHSessionImpl(Properties properties) {
		this.properties = properties;
	}

	/**
	 * Initializes channel with host and executes the commands. It's possible to
	 * add commands given as params
	 * 
	 * Example: exec("ls -lrt", "pwd");
	 * 
	 * @param cmds
	 *            Command/s given as strings
	 * @return Status code: 0 for success, 1 for completed with errors.
	 */
	@Override
	public int exec(String... cmds) {
		return exec(DEFAULT_TIMEOUT, cmds);
	}

	/**
	 * Initializes channel with host and executes the commands. It's possible to
	 * add commands given as params. Waits for the given time-out for the
	 * computation to complete.
	 * 
	 * Example: exec(5000, "ls -lrt", "pwd");
	 * 
	 * @param timeOut
	 *            Integer that indicates how much milliseconds channel needs to
	 *            wait for command execution
	 * @param cmds
	 *            Command/s given as strings
	 * @return Status code: 0 for success, 1 for completed with errors.
	 */
	@Override
	public int exec(int timeOut, String... cmds) {
		Session session = SessionFactory.createSession(this.properties);
		int exitStatus = -1;
		Channel channel = null;
		try {
			channel = session.openChannel(EXEC);
			logger.info("Open channel at [" + session.getHost() + PORT_STRING + session.getPort() + "]");
			String commands = String.join(DOUBLE_AMP, cmds);
			((ChannelExec) channel).setPty(true);
			((ChannelExec) channel).setCommand(commands);
			InputStream in = channel.getInputStream();
			InputStream inErr = (((ChannelExec) channel).getErrStream());
			long startTime = System.currentTimeMillis();
			channel.connect(timeOut);
			long endTime = System.currentTimeMillis();
			long counterTotal = (timeOut - (endTime - startTime)) / SLEEP_TIME;
			BufferedReader buffer = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
			BufferedReader bufferErr = new BufferedReader(new InputStreamReader(inErr, StandardCharsets.UTF_8));
			String result = buffer.lines().collect(Collectors.joining(StringUtils.LF));
			logger.info(result);
			int counter = 0;
			do {
				exitStatus = channel.getExitStatus();
				if (exitStatus == 1) {
					String errorMessage = bufferErr.lines().collect(Collectors.joining(StringUtils.LF));
					logger.error("Command execution failed on server [" + session.getHost() + "] port: ["
							+ session.getPort() + "] for the following reason: " + errorMessage);
				}
				Thread.sleep(SLEEP_TIME);
				counter++;
			} while ((!channel.isClosed() && counter <= counterTotal) || exitStatus == -1);
			if (channel != null && channel.isConnected()) {
				channel.disconnect();
				logger.info("Channel close at [" + session.getHost() + PORT_STRING + session.getPort() + "]");
			}
		} catch (IOException ioExc) {
			throw new SSHRuntimeException("Error creating inputstream to receive messages from the remote host ["
			        + session.getHost() + PORT_STRING + session.getPort() + "]", ioExc);
		} catch (InterruptedException interruptExc) {
			throw new SSHRuntimeException("Thread interrupted waiting for the termination of commands.", interruptExc);
		} catch (JSchException jschExc) {
			throw new SSHRuntimeException(
			        "Connection error to server [" + session.getHost() + PORT_STRING + session.getPort() + "]", jschExc);
		} finally {
			if (session != null && session.isConnected()) {
				session.disconnect();
			}
		}
		return exitStatus;
	}

	/**
	 * Initializes channel with host and execute asynchronously the commands.
	 * It's possible to add commands given as params.
	 * 
	 * Example: Future<Integer> result = execAsync("ls -lrt", "pwd"); Integer
	 * exitcode = result.get(5000, TimeUnit.MILLISECONDS);
	 * 
	 * @param cmds
	 *            Command/s given as strings
	 * 
	 * @return a Future that represents the result of an asynchronous
	 *         computation.
	 */
	@Override
	public Future<Integer> execAsync(String... cmds) {
		ExecutorService exec = Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
			public Thread newThread(Runnable r) {
				Thread t = Executors.defaultThreadFactory().newThread(r);
				t.setDaemon(true);
				return t;
			}
		});
		return exec.submit(() -> exec(0, cmds));
	}
}