package test.automation.core.ssh;

import java.util.concurrent.Future;

public interface SSHSession {
	/**
	 * Initializes channel with host and executes the commands. It's possible to
	 * add commands given as params
	 * 
	 * Example: exec("ls -lrt", "pwd");
	 * 
	 * @param cmds
	 *            Command/s given as strings
	 * @return Status code: 0 for success, 1 for completed with errors.
	 */
	int exec(String... cmd);

	/**
	 * Initializes channel with host and executes the commands. It's possible to
	 * add commands given as params. Waits for the given time-out for the
	 * computation to complete.
	 * 
	 * Example: exec(5000, "ls -lrt", "pwd");
	 * 
	 * @param timeOut
	 *            Integer that indicates how much milliseconds channel needs to
	 *            wait for command execution
	 * @param cmds
	 *            Command/s given as strings
	 * @return Status code: 0 for success, 1 for completed with errors.
	 */
	int exec(int timeOut, String... cmd);

	/**
	 * Initializes channel with host and execute asynchronously the commands.
	 * It's possible to add commands given as params.
	 * 
	 * Example: Future<Integer> result = execAsync("ls -lrt", "pwd"); Integer
	 * exitcode = result.get(5000, TimeUnit.MILLISECONDS);
	 * 
	 * @param cmds
	 *            Command/s given as strings
	 * 
	 * @return Status code: 0 for success, 1 for completed with errors.
	 */
	Future<Integer> execAsync(String... cmd);
}
