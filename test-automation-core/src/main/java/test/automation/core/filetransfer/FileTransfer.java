package test.automation.core.filetransfer;

public interface FileTransfer {
	/**
	 * upload file from local machine to remote Example:
	 * upload("localpath/filename.ext", "remotepath");
	 * 
	 * @param fromPath
	 * @param toPath
	 */
	void upload(String fromPath, String toPath);

	/**
	 * download file from remote machine Example:
	 * download("remotepath/filename.ext","localpath/filename.ext");
	 * 
	 * @param fromPath
	 * @param toPath
	 */
	void download(String fromPath, String toPath);
}
