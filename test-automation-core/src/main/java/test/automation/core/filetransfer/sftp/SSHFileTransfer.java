package test.automation.core.filetransfer.sftp;

import java.io.File;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.springframework.util.Assert;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import test.automation.core.filetransfer.FileTransfer;
import test.automation.core.ssh.SSHRuntimeException;
import test.automation.core.ssh.SessionFactory;

public class SSHFileTransfer implements FileTransfer {
	private static final String SFTP = "sftp";
	//Fix Sonar critical issue
	private static final String PORT_STRING = "] port [";
	private Properties properties;
	private Session session;
	private ChannelSftp sftpChannel;

	public SSHFileTransfer(Properties properties) {
		Assert.notNull(properties, "Properties have not been set!");
		this.properties = properties;
	}

	private void createChannel(Properties properties) {
		this.session = SessionFactory.createSession(properties);
		try {
			sftpChannel = (ChannelSftp) session.openChannel(SFTP);
			sftpChannel.connect();
		} catch (JSchException jschExc) {
			throw new SSHRuntimeException("Failed to open SFTP channel to server [" + this.session.getHost()
					+ PORT_STRING + this.session.getPort() + "]", jschExc);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void download(String fromPath, String toPath) {
		createChannel(this.properties);
		try {
			sftpChannel.get(FilenameUtils.normalize(fromPath, true), toPath);
		} catch (SftpException sftpExc) {
			throw new SSHRuntimeException("Failed to download the file from path: " + fromPath + ", from server ["
					+ this.session.getHost() + PORT_STRING + this.session.getPort() + "]", sftpExc);
		} finally {
			if (sftpChannel != null) {
				sftpChannel.disconnect();
			}
			session.disconnect();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void upload(String fromPath, String toPath) {
		createChannel(this.properties);
		try {
			sftpChannel.cd(toPath);
			File localFile = new File(fromPath);
			sftpChannel.put(localFile.getAbsolutePath(), localFile.getName());
		} catch (SftpException sftpExc) {
			throw new SSHRuntimeException("Failed to upload the file: " + fromPath + ", to server ["
					+ this.session.getHost() + PORT_STRING + this.session.getPort() + "]", sftpExc);
		} finally {
			if (sftpChannel != null) {
				sftpChannel.disconnect();
			}
			session.disconnect();
		}
	}
}