package test.automation.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import test.automation.core.benchmark.BenchmarkableFilter;
import test.automation.core.properties.FileMultiPropertiesLoaderImpl;
import test.automation.core.properties.MultiPropertiesLoader;
import test.automation.core.support.api.ApiConstants;

/*
 * This class provides utilities method to get pre-configured RestAssured client
 * instances using externalized properties.  
 */
public class ApiUtils {

	// create a specific HTTP request specification for every different server
	private Map<String, RequestSpecification> serverRequestsSpecifications;
	private final Map<String, Properties> propertiesBundles;
	private static ApiUtils instance;

	private static final Logger logger = LoggerFactory.getLogger(ApiUtils.class);


	/**
	 * Loading properties file from classpath.
	 */
	private ApiUtils() {

		serverRequestsSpecifications = new HashMap<String, RequestSpecification>();
		MultiPropertiesLoader multiPropertiesLoader = new FileMultiPropertiesLoaderImpl(
				(file) -> file.getName().split("-")[0]);

		propertiesBundles = multiPropertiesLoader.loadAndCheck((d, name) -> name.endsWith("-api.properties"),
				ApiConstants.BASE_URI);

		if (!propertiesBundles.containsKey(ApiConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY))
			throw new IllegalArgumentException("No default-api.properties found in the "
					+ ((FileMultiPropertiesLoaderImpl) multiPropertiesLoader).getPath());


		for (Entry<String, Properties> propertyBundle : propertiesBundles.entrySet()) {
			boolean authEncrypted = Boolean
					.parseBoolean(propertyBundle.getValue().getProperty(ApiConstants.HTTP_AUTH_ENCRYPTED));
			boolean bodyEncrypted = Boolean
					.parseBoolean(propertyBundle.getValue().getProperty(ApiConstants.HTTP_BODY_ENCRYPTED));

			RequestSpecification requestSpecification = initTemplate(propertyBundle.getKey(),
					propertyBundle.getValue());
			serverRequestsSpecifications.put(propertyBundle.getKey(), requestSpecification);

			Boolean isAuthOAM = Boolean.valueOf(propertyBundle.getValue().getProperty(ApiConstants.AUTH_OAM_ENABLED));
			Boolean isOAuth2 = Boolean.valueOf(propertyBundle.getValue().getProperty(ApiConstants.OAUTH2_ENABLED));

			// OAM authentication and OAuth2 authentication are not allowed
			// together

		}

	}

	/**
	 * singleton factory method
	 * 
	 * @return single instance of DatabaseUtils
	 */
	public static ApiUtils api() {
		if (instance != null) {
			return instance;
		}

		synchronized (ApiUtils.class) {
			if (instance != null) {
				return instance;
			}

			instance = new ApiUtils();
			return instance;

		}
	}

	/**
	 * returning the request template for default api server. It was always
	 * guaranteed to have a request template for default server
	 */
	public RequestSpecification template() {
		RequestSpecification requestSpecification = serverRequestsSpecifications
				.get(ApiConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY);

		final RequestSpecification request = RestAssured.given(requestSpecification).filter(new BenchmarkableFilter());
		boolean isEncryptBody = Boolean.parseBoolean(propertiesBundles
				.get(ApiConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY).getProperty(ApiConstants.HTTP_BODY_ENCRYPTED));
		boolean isAuthOAM = Boolean.parseBoolean(propertiesBundles.get(ApiConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY)
				.getProperty(ApiConstants.AUTH_OAM_ENABLED));
		Boolean isOAuth2 = Boolean.valueOf(propertiesBundles.get(ApiConstants.DEFAULT_API_PROPERTIES_BUNDLE_KEY)
				.getProperty(ApiConstants.OAUTH2_ENABLED));


		return request;
	}

	// TODO new feature: failfast option that will check current configuration
	// for template before any test is run. This will speed up the automatic
	// test execution fail as the check will happen before all tests innvocation
	// requests. (e.g. 10 tests --> 10 invocation requests--> 10 connection time
	// out to wait )

	public RequestSpecification template(String apiServer) {

		final RequestSpecification requestSpecification = serverRequestsSpecifications.get(apiServer);
		if (requestSpecification == null)
			throw new IllegalArgumentException("No request template available for api server " + apiServer);

		final RequestSpecification request = RestAssured.given(requestSpecification).filter(new BenchmarkableFilter());
		// check if body encryption is enabled
		boolean isEncryptBody = Boolean
				.parseBoolean(propertiesBundles.get(apiServer).getProperty(ApiConstants.HTTP_BODY_ENCRYPTED));
		boolean isAuthOAM = Boolean
				.parseBoolean(propertiesBundles.get(apiServer).getProperty(ApiConstants.AUTH_OAM_ENABLED));
		Boolean isOAuth2 = Boolean.valueOf(propertiesBundles.get(apiServer).getProperty(ApiConstants.OAUTH2_ENABLED));

		return request;

	}

	/**
	 * Create a RestAssured request specification. Currently only HTTP basic
	 * authentication is supported. If the property http.auth.encrypted is set
	 * to true, the request specification will contain an authorization header
	 * value crypted.
	 * 
	 * @param templateKey
	 *            if needed use to retrieve information from the maps e.g.
	 *            EncryptionManager
	 * @param properties
	 * @return
	 */
	private RequestSpecification initTemplate(String templateKey, Properties properties) {

		RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

		String baseuri = properties.getProperty(ApiConstants.BASE_URI);
		requestSpecBuilder.setBaseUri(baseuri);

		String username = properties.getProperty(ApiConstants.AUTH_BASIC_USERNAME);
		String password = properties.getProperty(ApiConstants.AUTH_BASIC_PASSWORD);

		Boolean usePreemptive = Boolean.valueOf(properties.getProperty(ApiConstants.AUTH_PREEMPTIVE));

		RequestSpecification build = requestSpecBuilder.build();



		return build;
	}

}