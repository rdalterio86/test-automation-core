package test.automation.core.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * When a class is annotated with <code>&#064;Ui</code>, the core will build the cucumber runtime with
 * all hooks for Ui test purpose
 * 
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Ui {}
