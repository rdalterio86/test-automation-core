package test.automation.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;
import java.util.Scanner;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

public class TestDataUtils {

	/**
	 * This method reads the a file containing some placeholders to be
	 * substituted with specific input parameters. If parameters are empty no
	 * replacement will happen.
	 * 
	 * @param parameters
	 * @param templatePath
	 * @return The request parametrized as String
	 * @throws IOException
	 * @throws ParseException
	 * @throws MalformedTemplateNameException
	 * @throws Exception
	 */

	public static String filter(Map<String, String> parameters, String templatePath) throws Exception {

		if (parameters == null || parameters.isEmpty()) {
			throw new IllegalArgumentException("empty paramters for template file "+templatePath);
		}
		Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
		cfg.setClassForTemplateLoading(TestDataUtils.class, "/");
		Template template = cfg.getTemplate(templatePath);
		/* Merge data-model with template */
		ByteArrayOutputStream sos = new ByteArrayOutputStream();
		Writer out = new OutputStreamWriter(sos);
		template.process(parameters, out);

		String request = sos.toString();

		return request;
	}

	/*
	 * Load file from class path and convert to a string
	 */
	public static String asString(String testDataPath) {
		testDataPath = new StringBuffer("/").append(testDataPath).toString();
		
		String fileText = null;
				
		try (InputStream testDataStream = TestDataUtils.class.getResourceAsStream(testDataPath);
				Scanner scanner = new Scanner(testDataStream, "UTF-8")) {
			fileText = scanner.useDelimiter("\\A").next();
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to load test data from " + testDataPath, e);
		}
		
		return fileText;
	}

}
