package test.automation.core.graph;

import java.util.concurrent.Future;
import java.util.Map;

/**
 * This interface defines the methods to traverse a graph using Gremlin Query
 * String pattern.
 * 
 *
 */
public interface GraphSessionClient<S, RS, F extends Future<RS>> {

	S getUnderlyingObject();

	RS executeGraph(String gremlinQuery);

	RS executeGraph(String gremlinQuery, Map<String, Object> parameters);

	F executeGraphAsync(String gremlinQuery, Map<String, Object> parameters);

}
