package test.automation.core.graph;

import java.io.Closeable;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;

/**
 * This interface defines the method to traverse a graph using Gremlin Java API Fluent.
 * 
 *
 */
public interface GraphSession extends Closeable {

	GraphTraversalSource G();

}
