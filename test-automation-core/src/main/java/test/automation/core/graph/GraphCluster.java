package test.automation.core.graph;

import java.io.Closeable;

/**
 * This interface defines the method to connect to a cluster graph database
 * platform.
 * 
 *
 */
public interface GraphCluster<T extends GraphSession> extends Closeable {

	T connect();
}
